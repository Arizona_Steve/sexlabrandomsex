#pragma once

#include "common/ITypes.h"
#include "skse64/PapyrusNativeFunctions.h"

namespace RndSexNpcAnimWrapper {
	void beginRefresh(StaticFunctionTag *base);
	void addEntry(StaticFunctionTag *base, UInt32 maleCount, UInt32 femaleCount);
	void completeRefresh(StaticFunctionTag *base);
	bool registerFunctions(VMClassRegistry* registry);
}
