#pragma warning(disable: 4244)

#include <random>
#include "windows.h"
#include "RndSexNpcVictimEval.h"

// Defined in RndSexEvaluator.cpp
extern std::default_random_engine random;

// Clears out any entries from a previous evaluation.
void RndSexNpcVictimEval::clear() {
	maleNpcList.clear();
	femaleNpcList.clear();
}

// Add an NPC to the male NPC list.
void RndSexNpcVictimEval::addMale(Actor *actor) {
	maleNpcList.addNpc(actor);
}

// Add an NPC to the female NPC list.
void RndSexNpcVictimEval::addFemale(Actor *actor) {
	femaleNpcList.addNpc(actor);
}

// Return the number of male NPCs.
size_t RndSexNpcVictimEval::getMaleCount() const {
	return maleNpcList.getNpcCount();
}

// Return the number of female NPCs.
size_t RndSexNpcVictimEval::getFemaleCount() const {
	return femaleNpcList.getNpcCount();
}

// Select a random NPC from the male or female NPC lists. Selection can be
// restricted to male or female NPCs based on the selectMale or selectFemale
// flags. Updates the femaleSelected flag to indicate the gender of the NPC
// selected.
Actor *RndSexNpcVictimEval::selectNpc(const bool selectMale, const bool selectFemale, bool &femaleSelected) {
	size_t maleCount = (selectMale) ? maleNpcList.getNpcCount() : 0;
	size_t totalCount = maleCount;

	if (selectFemale) {
		totalCount += femaleNpcList.getNpcCount();
	}

	if (totalCount == 0) {
		throw std::out_of_range("RndSexNpcVictimEval: No available NPCs");
	}

	size_t selectedIndex = 0;

	if (totalCount > 1) {
		std::uniform_int_distribution<size_t> distribution(0, totalCount - 1);
		selectedIndex = distribution(random);
	}

	if (selectedIndex < maleCount) {
		femaleSelected = false;
		return maleNpcList.getNpcAtIndex(selectedIndex);
	} else {
		femaleSelected = true;
		selectedIndex -= maleCount;
		return femaleNpcList.getNpcAtIndex(selectedIndex);
	}
}

// Restores deleted actors to the current lists following an unsuccessful
// evaluation.
void RndSexNpcVictimEval::restoreLists() {
	maleNpcList.restoreLists();
	femaleNpcList.restoreLists();
}
