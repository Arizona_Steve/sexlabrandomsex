#pragma once

#include "RndSexNpcAttackerEval.h"
#include "RndSexNpcVictimEval.h"

class RndSexNpcEval {
public:
	void clear();
	void addMaleAttacker(Actor *actor);
	void addMaleVictim(Actor *actor);
	void addFemaleAttacker(Actor *actor);
	void addFemaleVictim(Actor *actor);
	size_t getMaleCount() const;
	size_t getFemaleCount() const;
	void selectMasturbatorsForCreatureSex(VMResultArray<Actor *> &sexActorArray, const bool victimFemale);
	bool evaluateGroupSex(VMResultArray<Actor*> &sexActorArray, Actor *player, const bool playerFemale);
	bool evaluateCoupleSex(VMResultArray<Actor*> &sexActorArray, Actor *player, const bool playerFemale);
	void evaluateSoloSex(VMResultArray<Actor*>& sexActorArray);
	Actor *selectNpc(const bool maleAttacker, const bool femaleAttacker, const bool maleVictim, const bool femaleVictim, bool &victimFemale);
	VMResultArray<Actor*> getMasturbatorArray();
	void restoreLists();
private:
	RndSexNpcAttackerEval attackerEvaluator;
	RndSexNpcVictimEval victimEvaluator;
	Actor *selectVictimForGroupSex(Actor *player, const bool playerFemale, bool &victimFemale);
	Actor *selectVictimForCoupleSex(Actor *player, const bool playerFemale, bool &victimFemale);
};
