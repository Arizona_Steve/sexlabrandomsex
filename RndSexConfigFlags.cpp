#pragma warning(disable: 4244)

#include "RndSexConfigFlags.h"

RndSexConfigFlags::RndSexConfigFlags() :
	creatureMale(true),
	creatureFemale(true),
	groupAllMale(true),
	groupOneFemale(true),
	groupMixed(true),
	groupOneMale(true),
	groupAllFemale(true),
	coupleAllMale(true),
	coupleMixed(true),
	coupleAllFemale(true),
	soloMale(true),
	soloFemale(true) {
}

bool RndSexConfigFlags::isCreatureMale() const {
	return creatureMale;
}

void RndSexConfigFlags::setCreatureMale(const bool value) {
	creatureMale = value;
}

bool RndSexConfigFlags::isCreatureFemale() const {
	return creatureFemale;
}

void RndSexConfigFlags::setCreatureFemale(const bool value) {
	creatureFemale = value;
}

bool RndSexConfigFlags::isGroupAllMale() const {
	return groupAllMale;
}

void RndSexConfigFlags::setGroupAllMale(const bool value) {
	groupAllMale = value;
}

bool RndSexConfigFlags::isGroupOneFemale() const {
	return groupOneFemale;
}

void RndSexConfigFlags::setGroupOneFemale(const bool value) {
	groupOneFemale = value;
}

bool RndSexConfigFlags::isGroupMixed() const {
	return groupMixed;
}

void RndSexConfigFlags::setGroupMixed(const bool value) {
	groupMixed = value;
}

bool RndSexConfigFlags::isGroupOneMale() const {
	return groupOneMale;
}

void RndSexConfigFlags::setGroupOneMale(const bool value) {
	groupOneMale = value;
}

bool RndSexConfigFlags::isGroupAllFemale() const {
	return groupAllFemale;
}

void RndSexConfigFlags::setGroupAllFemale(const bool value) {
	groupAllFemale = value;
}

bool RndSexConfigFlags::isCoupleAllMale() const {
	return coupleAllMale;
}

void RndSexConfigFlags::setCoupleAllMale(const bool value) {
	coupleAllMale = value;
}

bool RndSexConfigFlags::isCoupleMixed() const {
	return coupleMixed;
}

void RndSexConfigFlags::setCoupleMixed(const bool value) {
	coupleMixed = value;
}

bool RndSexConfigFlags::isCoupleAllFemale() const {
	return coupleAllFemale;
}

void RndSexConfigFlags::setCoupleAllFemale(const bool value) {
	coupleAllFemale = value;
}

bool RndSexConfigFlags::isSoloMale() const {
	return soloMale;
}

void RndSexConfigFlags::setSoloMale(const bool value) {
	soloMale = value;
}

bool RndSexConfigFlags::isSoloFemale() const {
	return soloFemale;
}

void RndSexConfigFlags::setSoloFemale(const bool value) {
	soloFemale = value;
}

void RndSexConfigFlags::load(SKSESerializationInterface *serializer, UInt32 version) {
	serializer->ReadRecordData(&creatureMale, sizeof(creatureMale));
	serializer->ReadRecordData(&creatureFemale, sizeof(creatureFemale));
	serializer->ReadRecordData(&groupAllMale, sizeof(groupAllMale));
	serializer->ReadRecordData(&groupOneFemale, sizeof(groupOneFemale));
	serializer->ReadRecordData(&groupMixed, sizeof(groupMixed));
	serializer->ReadRecordData(&groupOneMale, sizeof(groupOneMale));
	serializer->ReadRecordData(&groupAllFemale, sizeof(groupAllFemale));
	serializer->ReadRecordData(&coupleAllMale, sizeof(coupleAllMale));
	serializer->ReadRecordData(&coupleMixed, sizeof(coupleMixed));
	serializer->ReadRecordData(&coupleAllFemale, sizeof(coupleAllFemale));
	serializer->ReadRecordData(&soloMale, sizeof(soloMale));
	serializer->ReadRecordData(&soloFemale, sizeof(soloFemale));
}

void RndSexConfigFlags::save(SKSESerializationInterface *serializer) const {
	serializer->WriteRecordData(&creatureMale, sizeof(creatureMale));
	serializer->WriteRecordData(&creatureFemale, sizeof(creatureFemale));
	serializer->WriteRecordData(&groupAllMale, sizeof(groupAllMale));
	serializer->WriteRecordData(&groupOneFemale, sizeof(groupOneFemale));
	serializer->WriteRecordData(&groupMixed, sizeof(groupMixed));
	serializer->WriteRecordData(&groupOneMale, sizeof(groupOneMale));
	serializer->WriteRecordData(&groupAllFemale, sizeof(groupAllFemale));
	serializer->WriteRecordData(&coupleAllMale, sizeof(coupleAllMale));
	serializer->WriteRecordData(&coupleMixed, sizeof(coupleMixed));
	serializer->WriteRecordData(&coupleAllFemale, sizeof(coupleAllFemale));
	serializer->WriteRecordData(&soloMale, sizeof(soloMale));
	serializer->WriteRecordData(&soloFemale, sizeof(soloFemale));
}
