#pragma warning(disable: 4244)

#include <shlobj.h>
#include <string>
#include "common/ITypes.h"
#include "common/IDebugLog.h"
#include "skse64_common/skse_version.h"
#include "skse64/PluginAPI.h"
#include "RndSexActivityWrapper.h"
#include "RndSexCreatureAnimWrapper.h"
#include "RndSexNpcAnimWrapper.h"
#include "RndSexConfigWrapper.h"
#include "RndSexEvaluatorWrapper.h"

const UInt32 DATA_VERSION = 2;

static void Serialization_Revert(SKSESerializationInterface* serializer) {
}

static void Serialization_Save(SKSESerializationInterface* serializer) {
	if (serializer->OpenRecord('CNFG', DATA_VERSION)) {
		RndSexConfigWrapper::save(serializer);
	}
}

static void Serialization_Load(SKSESerializationInterface* serializer) {
	UInt32	type;
	UInt32	version;
	UInt32	length;

	while (serializer->GetNextRecordInfo(&type, &version, &length)) {
		if ((length > 0) && (type == 'CNFG')) {
			RndSexConfigWrapper::load(serializer, version);
		} else {
			_MESSAGE("unhandled type 0x%08", type);
			return;
		}
	}
}

extern "C" {
	__declspec(dllexport) SKSEPluginVersionData SKSEPlugin_Version = {
		SKSEPluginVersionData::kVersion,
		1,
		"SexLab Random Sex",
		"Arizona_Steve",
		"srclaydon@gmail.com",
		0,
		SKSEPluginVersionData::kVersionIndependent_StructsPost629,
		{ RUNTIME_VERSION_1_6_640, 0 },
		0
	};

	__declspec(dllexport) bool SKSEPlugin_Load(const SKSEInterface* skse) {
		gLog.OpenRelative(CSIDL_MYDOCUMENTS, "\\My Games\\Skyrim Special Edition\\SKSE\\SexLabRandomSex.log");
		gLog.SetPrintLevel(IDebugLog::kLevel_Error);

		#ifdef _DEBUG
		gLog.SetLogLevel(IDebugLog::kLevel_DebugMessage);
		#else
		gLog.SetLogLevel(IDebugLog::kLevel_Message);
		#endif

		SKSEMessagingInterface* messaging = (SKSEMessagingInterface*)skse->QueryInterface(kInterface_Messaging);

		if (messaging == nullptr) {
			_WARNING("Failed to get SKSE Messaging interface");
			return false;
		}

		SKSEPapyrusInterface* papyrus = (SKSEPapyrusInterface*)skse->QueryInterface(kInterface_Papyrus);
		papyrus->Register(RndSexActivityWrapper::registerFunctions);
		papyrus->Register(RndSexCreatureAnimWrapper::registerFunctions);
		papyrus->Register(RndSexNpcAnimWrapper::registerFunctions);
		papyrus->Register(RndSexConfigWrapper::registerFunctions);
		papyrus->Register(RndSexEvaluatorWrapper::registerFunctions);

		PluginHandle pluginHandle = skse->GetPluginHandle();
		SKSESerializationInterface* serialization = (SKSESerializationInterface*)skse->QueryInterface(kInterface_Serialization);
		serialization->SetUniqueID(pluginHandle, 'RDSX');
		serialization->SetRevertCallback(pluginHandle, Serialization_Revert);
		serialization->SetSaveCallback(pluginHandle, Serialization_Save);
		serialization->SetLoadCallback(pluginHandle, Serialization_Load);
		return true;
	}
};
