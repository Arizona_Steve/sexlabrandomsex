#pragma once

#include <unordered_map>
#include <shared_mutex>
#include "common/ITypes.h"
#include "skse64/GameTypes.h"

typedef std::unordered_map<UInt32, UInt32> RndSexCreatureCountMap;
typedef std::unordered_map<std::string, RndSexCreatureCountMap> RndSexCreatureAnimMap;

class RndSexCreatureAnim {
public:
	RndSexCreatureAnim();
	~RndSexCreatureAnim();
	void beginRefresh();
	void addEntry(const BSFixedString &raceKey, const UInt32 creatureCount);
	void completeRefresh();
	const bool hasAnimations() const;
	const bool hasRaceKey(const std::string &raceKey) const;
	const RndSexCreatureCountMap getAnimationCountsByRaceKey(const std::string &raceKey) const;
private:
	mutable std::shared_mutex mainMutex;
	mutable std::mutex tempMutex;
	RndSexCreatureAnimMap *mainAnimationMap;
	RndSexCreatureAnimMap *tempAnimationMap;
};
