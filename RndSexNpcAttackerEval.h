#pragma once

#include "RndSexNpcVictimEval.h"

class RndSexNpcAttackerEval:public RndSexNpcVictimEval {
public:
	virtual void clear();
	UInt32 determineGroupSexCount();
	void selectAttackersForGroupSex(VMResultArray<Actor*> &sexActorArray, Actor *victim, const bool victimFemale, const UInt32 npcCount, UInt32 &femaleCount);
	void selectAttackerForCoupleSex(VMResultArray<Actor*> &sexActorArray, Actor *victim, const bool victimFemale, UInt32 &femaleCount);
	void selectMasturbatorsForCreatureSex(VMResultArray<Actor *> &sexActorArray, const bool victimFemale);
	void selectMasturbatorsForGroupSex(VMResultArray<Actor *> &sexActorArray, const UInt32 npcCount, const UInt32 femaleCount);
	void selectMasturbatorsForCoupleSex(VMResultArray<Actor *> &sexActorArray, const UInt32 femaleCount);
	void selectMasturbatorsForSoloSex(VMResultArray<Actor *> &sexActorArray, const bool victimFemale);
	void selectMasturbators(const bool selectMale, const bool selectFemale);
	VMResultArray<Actor*> getMasturbatorArray();
private:
	VMResultArray<Actor*> masturbatorArray;
};
