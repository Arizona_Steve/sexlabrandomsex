#pragma warning(disable: 4244)

#include "windows.h"
#include "common/ITypes.h"
#include "RndSexNpcList.h"

// Clears out any entries from a previous evaluation.
void RndSexNpcList::clear() {
	currentNpcList.clear();
	deletedNpcList.clear();
}

// Add an NPC to the list.
void RndSexNpcList::addNpc(Actor *actor) {
	if (actor != nullptr) {
		currentNpcList.push_back(actor);
	}
}

// Return the number of NPCs in the list.
size_t RndSexNpcList::getNpcCount() const {
	return currentNpcList.size();
}

// Selects the actor at the specified index from the specified current actor
// list. If the index is greater than the last list index, then return null.
// The selected actor is removed from the current actor list and saved to a
// deleted actor list. For speed, actor removal is achieved by copying the
// actor at the end of the list to the specified index and popping the last
// element. Throws out_of_range if an invalid selected index is specified.
Actor *RndSexNpcList::getNpcAtIndex(const size_t selectedIndex) {
	const size_t currentSize = currentNpcList.size();

	if (selectedIndex >= currentSize) {
		throw std::out_of_range("RndSexNpcList: Selected index out of range");
	}

	size_t lastListIndex = currentSize - 1;
	Actor *selectedActor = currentNpcList[selectedIndex];

	if (lastListIndex == 0) {
		currentNpcList.clear();
	} else if (selectedIndex == lastListIndex) {
		currentNpcList.pop_back();
	} else {
		currentNpcList[selectedIndex] = currentNpcList[lastListIndex];
		currentNpcList.pop_back();
	}

	deletedNpcList.push_back(selectedActor);
	return selectedActor;
}

// Move any deleted NPCs back to the current NPC list.
void RndSexNpcList::restoreLists() {
	currentNpcList.insert(currentNpcList.end(), deletedNpcList.begin(), deletedNpcList.end());
	deletedNpcList.clear();
}
