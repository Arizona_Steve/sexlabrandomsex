#pragma once

#include "common/ITypes.h"

class RndSexNpcAnimCounts {
public:
	RndSexNpcAnimCounts();
	void clear();
	void incrementOneMaleCount();
	void incrementOneFemaleCount();
	void incrementTwoNpcCount();
	void incrementThreeNpcCount();
	void incrementFourNpcCount();
	void incrementFiveNpcCount();
	UInt32 getOneMaleCount() const;
	UInt32 getOneFemaleCount() const;
	UInt32 getTwoNpcCount() const;
	UInt32 getThreeNpcCount() const;
	UInt32 getFourNpcCount() const;
	UInt32 getFiveNpcCount() const;
	bool hasSoloMaleAnimations() const;
	bool hasSoloFemaleAnimations() const;
	bool hasCoupleAnimations() const;
	bool hasGroupAnimations() const;
private:
	UInt32 oneMaleCount;
	UInt32 oneFemaleCount;
	UInt32 twoNpcCount;
	UInt32 threeNpcCount;
	UInt32 fourNpcCount;
	UInt32 fiveNpcCount;
};
