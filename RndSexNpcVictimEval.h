#pragma once

#include <vector>
#include "common/ITypes.h"
#include "skse64/GameReferences.h"
#include "skse64/PapyrusArgs.h"
#include "RndSexNpcList.h"

class RndSexNpcVictimEval {
public:
	virtual void clear();
	void addMale(Actor *actor);
	void addFemale(Actor *actor);
	size_t getMaleCount() const;
	size_t getFemaleCount() const;
	Actor *selectNpc(const bool selectMale, const bool selectFemale, bool &femaleSelected);
	void restoreLists();
protected:
	RndSexNpcList maleNpcList;
	RndSexNpcList femaleNpcList;
};
