#pragma warning(disable: 4244)

#include "windows.h"
#include "RndSexCreatureAnim.h"
#include "RndSexCreatureAnimWrapper.h"

RndSexCreatureAnim creatureAnimations;

namespace RndSexCreatureAnimWrapper {
	void beginRefresh(StaticFunctionTag *base) {
		creatureAnimations.beginRefresh();
	}

	void addEntry(StaticFunctionTag *base, BSFixedString raceKey, UInt32 creatureCount) {
		if ((strlen(raceKey.data) > 0) && (creatureCount > 0)) {
			creatureAnimations.addEntry(raceKey, creatureCount);
		}
	}

	void completeRefresh(StaticFunctionTag *base) {
		return creatureAnimations.completeRefresh();
	}

	// Animation functions implement their own locking, so are threadsafe and
	// can be set up with the NoWait parameter.
	bool registerFunctions(VMClassRegistry *registry) {
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, void>("beginRefresh", "RndSexCreatureAnimQuest", beginRefresh, registry));
		registry->RegisterFunction(new NativeFunction2<StaticFunctionTag, void, BSFixedString, UInt32>("addEntry", "RndSexCreatureAnimQuest", addEntry, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, void>("completeRefresh", "RndSexCreatureAnimQuest", completeRefresh, registry));

		registry->SetFunctionFlags("RndSexCreatureAnimQuest", "beginRefresh", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexCreatureAnimQuest", "addEntry", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexCreatureAnimQuest", "completeRefresh", VMClassRegistry::kFunctionFlag_NoWait);
		return true;
	}
}
