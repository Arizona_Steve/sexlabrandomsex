#pragma once

#include <map>
#include <vector>
#include "skse64/PapyrusArgs.h"
#include "skse64/GameReferences.h"
#include "RndSexCreatureMap.h"

class RndSexCreatureEval {
public:
	void clear();
	void addCreature(const char *raceKey, Actor *actor);
	bool canEvaluate(const Actor *player, const bool playerFemale, const size_t maleCount, const size_t femaleCount, bool &usePlayer);
	std::string getValidRaceKey();
	UInt32 determineCount(const std::string &raceKey);
	void selectAttackers(VMResultArray<Actor *> &sexActorArray, const std::string &raceKey, const UInt32 creatureCount, Actor *victim);
private:
	RndSexCreatureMap creatureMap;
};
