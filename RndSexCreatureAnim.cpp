#pragma warning(disable: 4244)

#include "windows.h"
#include "RndSexCreatureAnim.h"

// Constructor.
RndSexCreatureAnim::RndSexCreatureAnim() :
	mainAnimationMap(nullptr),
	tempAnimationMap(nullptr) {
}

// Destructor.
RndSexCreatureAnim::~RndSexCreatureAnim() {
	if (mainAnimationMap != nullptr) {
		delete mainAnimationMap;
	}

	if (tempAnimationMap != nullptr) {
		delete tempAnimationMap;
	}
}

// Initialize the temporary creature animation map for loading. Typically
// animations are loaded into a temporary map and swapped into the animation map
// once loading is complete.
void RndSexCreatureAnim::beginRefresh() {
	std::unique_lock<std::mutex> lockTemp(this->tempMutex);

	try {
		if (tempAnimationMap == nullptr) {
			tempAnimationMap = new RndSexCreatureAnimMap;
		} else {
			tempAnimationMap->clear();
		}
	} catch (...) {
	}
}

// Adds a creature entry into the tempoarary animation map. 
void RndSexCreatureAnim::addEntry(const BSFixedString &raceKey, const UInt32 creatureCount) {
	std::unique_lock<std::mutex> lockTemp(this->tempMutex);

	if (tempAnimationMap != nullptr) {
		try {
			(*tempAnimationMap)[std::string(raceKey.data)][creatureCount] += 1;
		} catch (...) {
		}
	}
}

// Completes a creature animation refresh by removing the exising animation map
// and swapping in the temporary map that has been loaded with data.
void RndSexCreatureAnim::completeRefresh() {
	std::unique_lock<std::mutex> lockTemp(this->tempMutex);

	if (tempAnimationMap != nullptr) {
		std::unique_lock<std::shared_mutex> lockMain(this->mainMutex);

		if (mainAnimationMap != nullptr) {
			delete mainAnimationMap;
		}

		mainAnimationMap = tempAnimationMap;
		tempAnimationMap = nullptr;
	}
}

// Return true if creature animations have been loaded.
const bool RndSexCreatureAnim::hasAnimations() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return ((mainAnimationMap != nullptr) && !mainAnimationMap->empty());
}

// Return true if the specified race key has available animations.
const bool RndSexCreatureAnim::hasRaceKey(const std::string & raceKey) const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationMap == nullptr) ? false : (mainAnimationMap->count(raceKey) != 0);
}

// Returns an iterator if the specified race key is present in the animation
// map. If no animation map is loaded or the race key is not present, an
// out_of_range exception will be thrown. Note that a copy of the animation
// counts are returned for thread safety.
const RndSexCreatureCountMap RndSexCreatureAnim::getAnimationCountsByRaceKey(const std::string &raceKey) const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);

	if (mainAnimationMap == nullptr) {
		throw std::out_of_range("RndSexCreatureCountMap: Animation map not loaded");
	} else {
		return mainAnimationMap->at(raceKey);
	}
}
