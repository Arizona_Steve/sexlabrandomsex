#pragma once

#include "common/ITypes.h"
#include "skse64/PapyrusNativeFunctions.h"

namespace RndSexCreatureAnimWrapper {
	void beginRefresh(StaticFunctionTag *base);
	void addEntry(StaticFunctionTag *base, BSFixedString raceKey, UInt32 creatureCount);
	void completeRefresh(StaticFunctionTag *base);
	bool registerFunctions(VMClassRegistry* registry);
};
