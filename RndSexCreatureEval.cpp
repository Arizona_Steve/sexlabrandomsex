#pragma warning(disable: 4244)

#include <random>
#include "windows.h"
#include "common/ITypes.h"
#include "RndSexConfig.h"
#include "RndSexCreatureAnim.h"
#include "RndSexCreatureEval.h"

// Defined in RndSexConfigWrapper.cpp
extern RndSexConfig config;

// Defined in RndSexCreatureAnimWrapper.cpp
extern RndSexCreatureAnim creatureAnimations;

// Defined in RndSexEvaluator.cpp
extern std::default_random_engine random;
extern std::uniform_real_distribution<float> percentDistribution;

// Clear out any existing entries from the collections.
void RndSexCreatureEval::clear() {
	creatureMap.clear();
}

// Adds a creature to the race key list and maps the race key to a list of
// creatures having that race key.
void RndSexCreatureEval::addCreature(const char *raceKey, Actor *actor) {
	creatureMap.addCreature(raceKey, actor);
}

// Determines if a creature sex act is available based on the count of available
// creatures and NPCs, and whether or not the player can be involved. Also sets
// the use player flag to indicate that the player will be used in evaluations.
bool RndSexCreatureEval::canEvaluate(const Actor *player, const bool playerFemale, const size_t maleCount, const size_t femaleCount, bool &usePlayer) {
	if (config.isCreatureSexAvailable() && creatureAnimations.hasAnimations() && creatureMap.hasCreatures()) {
		if ((!config.isGroupSexAvailable() && !config.isCoupleSexAvailable() && !config.isSoloSexAvailable()) || (percentDistribution(random) < config.getPercentCreatureSex())) {
			bool creatureOnMale = config.getSexActFlags().isCreatureMale();
			bool creatureOnFemale = config.getSexActFlags().isCreatureFemale();

			if (player != nullptr) {
				if (config.isPlayerOnly()) {
					usePlayer = true;
					return (playerFemale ? creatureOnFemale : creatureOnMale);
				} else if (percentDistribution(random) < config.getPercentCreatureOnPlayer()) {
					if (playerFemale ? creatureOnFemale : creatureOnMale) {
						usePlayer = true;
						return true;
					}
				}
			}

			usePlayer = false;
			return ((creatureOnMale && (maleCount > 0)) || (creatureOnFemale && (femaleCount > 0)));
		}
	}

	return false;
}

// Selects a random race key from the creature race key list, ensuring that the
// selected race key has associated animations.
std::string RndSexCreatureEval::getValidRaceKey() {
	return creatureMap.getValidRaceKey();
}

// Randomly determines a creature count for the sex act based on the number of
// creatures available and the number of animations available for each creature
// count.
UInt32 RndSexCreatureEval::determineCount(const std::string &raceKey) {
	const RndSexCreatureActorList creatureList = creatureMap.getCreaturesByRaceKey(raceKey);
	const RndSexCreatureCountMap animationCountList = creatureAnimations.getAnimationCountsByRaceKey(raceKey);
	UInt32 creatureCount = (UInt32)creatureList.size();
	UInt32 oneCreatureAnimations = 0;
	UInt32 twoCreatureAnimations = 0;
	UInt32 threeCreatureAnimations = 0;
	UInt32 totalCreatureAnimations = 0;

	for (RndSexCreatureCountMap::const_iterator iterator = animationCountList.cbegin(); iterator != animationCountList.cend(); ++iterator) {
		const UInt32 creaturesInAnimation = iterator->first;

		if (creaturesInAnimation == 1) {
			if (creatureCount >= 1) {
				oneCreatureAnimations = iterator->second;
				totalCreatureAnimations += oneCreatureAnimations;
			}
		} else if (creaturesInAnimation == 2) {
			if (creatureCount >= 2) {
				twoCreatureAnimations = iterator->second;
				totalCreatureAnimations += twoCreatureAnimations;
			}
		} else if (creaturesInAnimation == 3) {
			if (creatureCount >= 3) {
				threeCreatureAnimations = iterator->second;
				totalCreatureAnimations += threeCreatureAnimations;
			}
		} else if (creaturesInAnimation == 4) {
			if (creatureCount >= 4) {
				totalCreatureAnimations += iterator->second;
			}
		}
	}

	if (totalCreatureAnimations == 0) {
		throw std::out_of_range("RndSexCreatureEval: No animations found for the specified creature race key");
	}

	UInt32 selectedIndex = 0;

	if (totalCreatureAnimations > 1) {
		std::uniform_int_distribution<UInt32> distribution(0, totalCreatureAnimations - 1);
		selectedIndex = distribution(random);
	}

	if (selectedIndex < oneCreatureAnimations) {
		return 1;
	}

	selectedIndex -= oneCreatureAnimations;

	if (selectedIndex < twoCreatureAnimations) {
		return 2;
	} else {
		selectedIndex -= twoCreatureAnimations;
		return (selectedIndex < threeCreatureAnimations) ? 3 : 4;
	}
}

// Randomly selects the creatures that will partake in the sex act and places
// them into an array which will eventually be returned to the Papyrus script.
void RndSexCreatureEval::selectAttackers(VMResultArray<Actor *> &sexActorArray, const std::string &raceKey, const UInt32 creatureCount, Actor *victim) {
	RndSexCreatureActorList availableCreatureList = creatureMap.getCreaturesByRaceKey(raceKey);
	UInt32 availableCreatureCount = (UInt32)availableCreatureList.size();

	if (availableCreatureCount < creatureCount) {
		throw std::out_of_range("RndSexCreatureEval: insufficient creatures to satisfy attacker count");
	}

	sexActorArray.resize(static_cast<size_t>(creatureCount) + 1, nullptr);
	sexActorArray[0] = victim;

	if (availableCreatureCount == creatureCount) {
		for (UInt32 attackerIndex = 1; attackerIndex <= creatureCount; ++attackerIndex) {
			sexActorArray[attackerIndex] = availableCreatureList[attackerIndex - 1];
		}
	} else {
		for (UInt32 attackerIndex = 1; attackerIndex <= creatureCount; ++attackerIndex) {
			UInt32 selectedIndex = 0;
			--availableCreatureCount;

			if (availableCreatureCount > 0) {
				std::uniform_int_distribution<UInt32> distribution(0, availableCreatureCount);
				selectedIndex = distribution(random);
			}

			sexActorArray[attackerIndex] = availableCreatureList[selectedIndex];

			if (availableCreatureCount == 0) {
				availableCreatureList.clear();
			} else if (selectedIndex < availableCreatureCount) {
				availableCreatureList[selectedIndex] = availableCreatureList[availableCreatureCount];
				availableCreatureList.pop_back();
			} else {
				availableCreatureList.pop_back();
			}
		}
	}
}
