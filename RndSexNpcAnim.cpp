#pragma warning(disable: 4244)

#include "windows.h"
#include "RndSexNpcAnim.h"

// Constructor.
RndSexNpcAnim::RndSexNpcAnim() :
	mainAnimationCounts(nullptr),
	tempAnimationCounts(nullptr) {
}

// Destructor.
RndSexNpcAnim::~RndSexNpcAnim() {
	if (mainAnimationCounts != nullptr) {
		delete mainAnimationCounts;
	}

	if (tempAnimationCounts != nullptr) {
		delete tempAnimationCounts;
	}
}

// Initialize the temporary NPC animation map for loading. Typically animations
// are loaded into a temporary map and swapped into the animation map once
// loading is complete.
void RndSexNpcAnim::beginRefresh() {
	std::unique_lock<std::mutex> lockTemp(this->tempMutex);

	if (tempAnimationCounts == nullptr) {
		tempAnimationCounts = new RndSexNpcAnimCounts;
	} else {
		tempAnimationCounts->clear();
	}
}

// Adds an NPC entry into the temporary animation map. 
void RndSexNpcAnim::addEntry(UInt32 maleCount, UInt32 femaleCount) {
	std::unique_lock<std::mutex> lockTemp(this->tempMutex);

	if (tempAnimationCounts != nullptr) {
		UInt32 totalCount = maleCount + femaleCount;

		if (totalCount == 1) {
			if (maleCount == 1) {
				tempAnimationCounts->incrementOneMaleCount();
			} else {
				tempAnimationCounts->incrementOneFemaleCount();
			}
		} else if (totalCount == 2) {
			tempAnimationCounts->incrementTwoNpcCount();
		} else if (totalCount == 3) {
			tempAnimationCounts->incrementThreeNpcCount();
		} else if (totalCount == 4) {
			tempAnimationCounts->incrementFourNpcCount();
		} else if (totalCount == 5) {
			tempAnimationCounts->incrementFiveNpcCount();
		}
	}
}

// Completes an NPC animation refresh by removing the exising animation map and
// swapping in the temporary map that has been loaded with data.
void RndSexNpcAnim::completeRefresh() {
	std::unique_lock<std::mutex> lockTemp(this->tempMutex);

	if (tempAnimationCounts != nullptr) {
		std::unique_lock<std::shared_mutex> lockMain(this->mainMutex);

		if (mainAnimationCounts != nullptr) {
			delete mainAnimationCounts;
		}

		mainAnimationCounts = tempAnimationCounts;
		tempAnimationCounts = nullptr;
	}
}

// Return the count of male masturbation animations.
UInt32 RndSexNpcAnim::getOneMaleCount() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->getOneMaleCount();
}

// Return the count of female masturbation animations.
UInt32 RndSexNpcAnim::getOneFemaleCount() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->getOneFemaleCount();
}

// Return the count of two NPC animations.
UInt32 RndSexNpcAnim::getTwoNpcCount() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->getTwoNpcCount();
}

// Return the count of three NPC animations.
UInt32 RndSexNpcAnim::getThreeNpcCount() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->getThreeNpcCount();
}

// Return the count of four NPC animations.
UInt32 RndSexNpcAnim::getFourNpcCount() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->getFourNpcCount();
}

// Return the count of five NPC animations.
UInt32 RndSexNpcAnim::getFiveNpcCount() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->getFiveNpcCount();
}

// Return true if there are one or more solo animations.
bool RndSexNpcAnim::hasSoloMaleAnimations() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->hasSoloMaleAnimations();
}

// Return true if there are one or more solo animations.
bool RndSexNpcAnim::hasSoloFemaleAnimations() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->hasSoloFemaleAnimations();
}

// Return true if there are one or more couple animations.
bool RndSexNpcAnim::hasCoupleAnimations() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->hasCoupleAnimations();
}

// Return true if there are one or more group sex animations.
bool RndSexNpcAnim::hasGroupAnimations() const {
	std::shared_lock<std::shared_mutex> lockMain(this->mainMutex);
	return (mainAnimationCounts == nullptr) ? 0 : mainAnimationCounts->hasGroupAnimations();
}
