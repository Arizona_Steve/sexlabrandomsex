#pragma warning(disable: 4244)

#include "windows.h"
#include "RndSexNpcAnim.h"
#include "RndSexNpcAnimWrapper.h"

RndSexNpcAnim npcAnimations;

namespace RndSexNpcAnimWrapper {
	void beginRefresh(StaticFunctionTag *base) {
		npcAnimations.beginRefresh();
	}

	void addEntry(StaticFunctionTag *base, UInt32 maleCount, UInt32 femaleCount) {
		if ((maleCount >= 0) && (femaleCount >= 0) && ((maleCount + femaleCount) > 0)) {
			npcAnimations.addEntry(maleCount, femaleCount);
		}
	}

	void completeRefresh(StaticFunctionTag *base) {
		npcAnimations.completeRefresh();
	}

	// Animation functions implement their own locking, so are threadsafe and
	// can be set up with the NoWait parameter.
	bool registerFunctions(VMClassRegistry *registry) {
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, void>("beginRefresh", "RndSexNpcAnimQuest", beginRefresh, registry));
		registry->RegisterFunction(new NativeFunction2<StaticFunctionTag, void, UInt32, UInt32>("addEntry", "RndSexNpcAnimQuest", addEntry, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, void>("completeRefresh", "RndSexNpcAnimQuest", completeRefresh, registry));

		registry->SetFunctionFlags("RndSexNpcAnimQuest", "beginRefresh", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexNpcAnimQuest", "addEntry", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexNpcAnimQuest", "completeRefresh", VMClassRegistry::kFunctionFlag_NoWait);
		return true;
	}
}