#pragma once

#include <atomic>
#include "common/ITypes.h"
#include "skse64/PluginAPI.h"

class RndSexConfigFlags {
public:
	RndSexConfigFlags();

	bool isCreatureMale() const;
	void setCreatureMale(const bool value);
	bool isCreatureFemale() const;
	void setCreatureFemale(const bool value);
	bool isGroupAllMale() const;
	void setGroupAllMale(const bool value);
	bool isGroupOneFemale() const;
	void setGroupOneFemale(const bool value);
	bool isGroupMixed() const;
	void setGroupMixed(const bool value);
	bool isGroupOneMale() const;
	void setGroupOneMale(const bool value);
	bool isGroupAllFemale() const;
	void setGroupAllFemale(const bool value);
	bool isCoupleAllMale() const;
	void setCoupleAllMale(const bool value);
	bool isCoupleMixed() const;
	void setCoupleMixed(const bool value);
	bool isCoupleAllFemale() const;
	void setCoupleAllFemale(const bool value);
	bool isSoloMale() const;
	void setSoloMale(const bool value);
	bool isSoloFemale() const;
	void setSoloFemale(const bool value);

	void load(SKSESerializationInterface *serializer, UInt32 version);
	void save(SKSESerializationInterface *serializer) const;
private:
	bool creatureMale;
	bool creatureFemale;
	bool groupAllMale;
	bool groupOneFemale;
	bool groupMixed;
	bool groupOneMale;
	bool groupAllFemale;
	bool coupleAllMale;
	bool coupleMixed;
	bool coupleAllFemale;
	bool soloMale;
	bool soloFemale;
};
