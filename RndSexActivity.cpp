#pragma warning(disable: 4244)
#pragma warning(disable: 4267)

#include <Windows.h>
#include <codecvt>
#include <sstream>
#include <common/ITypes.h>
#include <common/IDebugLog.h>
#include "RndSexActivity.h"
#include <skse64/ScaleformLoader.h>

/******************************************************************************/
/* Utility class to generate sex act messages. The messages are specified in  */
/* the translation file for each language and include {} placeholders to      */
/* indicate where actor names should be placed.                               */
/******************************************************************************/

static constexpr wchar_t* ONLOOKER = L"$RndSexOnlookerNotify";
static constexpr wchar_t* ONE_ACTOR = L"$RndSexMasturbatorNotify";
static constexpr wchar_t* TWO_ACTORS = L"$RndSexTwoActorNotify";
static constexpr wchar_t* THREE_ACTORS = L"$RndSexThreeActorNotify";
static constexpr wchar_t* FOUR_ACTORS = L"$RndSexFourActorNotify";
static constexpr wchar_t* FIVE_ACTORS = L"$RndSexFiveActorNotify";

static const char* ERROR_EXCEPTION = "RndSexActivity: Exception occurred during evaluation: %s";
static const char* ERROR_TRANSLATOR_LOAD = "RndSexActivity: Failed to load translator.";
static const char* ERROR_NO_CACHED_STRING = "RndSexActivity: Failed to retrieve cached string for %s";
static const char* ERROR_NO_TRANSLATOR_ITEM = "RndSexActivity: Failed to load translator item for message key %s";
static const char* ERROR_INVALID_ACTOR_COUNT = "RndSexActivity: Invalid number of actors in actor name array, no message generated";
static const char* ERROR_PLACEHOLDER_MISMATCH = "RndSexActivity: Occurrences of {} in translation string does not match actor count, no message generated";
static const char* LOADED_TRANSLATIONS = "RndSexActivity: Translation templates loaded successfully";

// Constructor.
RndSexActivity::RndSexActivity() :
	translator{ nullptr } {
}

// Generates a translated sex act notification message.
std::string RndSexActivity::getMessage(VMArray<BSFixedString>& actorNameArray, const bool masturbatingOnlooker) {
	if (translator == nullptr) {
		translator = loadTranslator();

		if (translator == nullptr) {
			return "";
		}
	}

	const UInt32 actorNameCount = actorNameArray.Length();
	std::string translation = "";

	switch (actorNameCount) {
	case 1:
		translation = getTranslationTemplate(translator, (masturbatingOnlooker ? ONLOOKER : ONE_ACTOR));
		break;
	case 2:
		translation = getTranslationTemplate(translator, TWO_ACTORS);
		break;
	case 3:
		translation = getTranslationTemplate(translator, THREE_ACTORS);
		break;
	case 4:
		translation = getTranslationTemplate(translator, FOUR_ACTORS);
		break;
	case 5:
		translation = getTranslationTemplate(translator, FIVE_ACTORS);
		break;
	default:
		_DMESSAGE(ERROR_INVALID_ACTOR_COUNT);
		return "";
	}

	std::stringstream buffer;
	size_t start = 0;
	size_t end = 0;

	for (UInt32 i = 0; i < actorNameCount; ++i) {
		end = translation.find("{}", start);

		if (end == std::string::npos) {
			_DMESSAGE(ERROR_PLACEHOLDER_MISMATCH);
			return "";
		}

		BSFixedString actorArrayValue;
		actorNameArray.Get(&actorArrayValue, i);
		buffer << translation.substr(start, end - start);
		buffer << actorArrayValue.c_str();
		start = end + 2;
	}

	if (translation.find("{}", start) == std::string::npos) {
		buffer << translation.substr(start);
		return buffer.str();
	} else {
		_DMESSAGE(ERROR_PLACEHOLDER_MISMATCH);
		return "";
	}
}

// Retrieve the translator.
BSScaleformTranslator* RndSexActivity::loadTranslator() {
	const GFxLoader* const loader = GFxLoader::GetSingleton();

	if (loader != nullptr) {
		GFxStateBag* const stateBag = loader->stateBag;

		if (stateBag != nullptr) {
			BSScaleformTranslator* translator = stateBag->GetTranslator();

			if (translator != nullptr) {
				return translator;
			}
		}
	}

	_WARNING(ERROR_TRANSLATOR_LOAD);
	return nullptr;
}

// Called by injectTranslator to retrieve a translation string from the
// translation table.
std::string RndSexActivity::getTranslationTemplate(BSScaleformTranslator* translator, wchar_t* templateKey) {
	wchar_t* key = nullptr;
	BSScaleformTranslator::GetCachedString(&key, templateKey, 0);
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;

	if (key == nullptr) {
		_DMESSAGE(ERROR_NO_CACHED_STRING, converter.to_bytes(templateKey).c_str());
		return "";
	}

	TranslationTableItem* item = translator->translations.Find((const wchar_t**)&key);

	if ((item == nullptr) || (item->translation == nullptr)) {
		_DMESSAGE(ERROR_NO_TRANSLATOR_ITEM, converter.to_bytes(templateKey).c_str());
		return "";
	}

	return converter.to_bytes(item->translation);
}
