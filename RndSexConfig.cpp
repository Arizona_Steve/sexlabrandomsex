#pragma warning(disable: 4244)

#include "RndSexConfig.h"

RndSexConfig::RndSexConfig() :
	questActive(false),
	showNotifications(true),
	allowOldPeople(true),
	allowDragons(true),
	allowBeastRaces(true),
	maleInMalePosition(true),
	useSexLabBedSetting(false),
	calmAfterSex(true),
	playerOnly(false),
	noTimeRestriction(true),
	followerOptionIndex(MIN_FOLLOWER_OPTION_INDEX),
	arousalMinimum(MIN_AROUSAL),
	masturbatorMinimum(MIN_MASTURBATOR),
	masturbatorMaximum(MIN_MASTURBATOR),
	activityStartHour(MIN_ACTIVITY_HOUR),
	activityEndHour(MAX_ACTIVITY_HOUR),
	maxDistanceMeters(DEFAULT_DISTANCE_METERS),
	percentCreatureSex(DEFAULT_PERCENTAGE),
	percentGroupSex(DEFAULT_PERCENTAGE),
	percentCoupleSex(DEFAULT_PERCENTAGE),
	percentMasturbator(DEFAULT_PERCENTAGE),
	percentCreatureOnPlayer(DEFAULT_PERCENTAGE),
	percentNpcOnPlayer(DEFAULT_PERCENTAGE),
	intervalMinimumMinutes(DEFAULT_INTERVAL_MINUTE),
	intervalMinimumSeconds(MIN_INTERVAL_SECOND),
	intervalMaximumMinutes(DEFAULT_INTERVAL_MINUTE),
	intervalMaximumSeconds(MIN_INTERVAL_SECOND) {
}

bool RndSexConfig::isQuestActive() const {
	return questActive;
}

void RndSexConfig::setQuestActive(const bool value) {
	questActive = value;
}

bool RndSexConfig::isShowNotifications() const {
	return showNotifications;
}

void RndSexConfig::setShowNotifications(const bool value) {
	showNotifications = value;
}

bool RndSexConfig::isAllowOldPeople() const {
	return allowOldPeople;
}

void RndSexConfig::setAllowOldPeople(const bool value) {
	allowOldPeople = value;
}

bool RndSexConfig::isAllowDragons() const {
	return allowDragons;
}

void RndSexConfig::setAllowDragons(const bool value) {
	allowDragons = value;
}

bool RndSexConfig::isMaleInMalePosition() const {
	return maleInMalePosition;
}

void RndSexConfig::setMaleInMalePosition(const bool value) {
	maleInMalePosition = value;
}

bool RndSexConfig::isAllowBeastRaces() const {
	return allowBeastRaces;
}

void RndSexConfig::setAllowBeastRaces(const bool value) {
	allowBeastRaces = value;
}

bool RndSexConfig::isUseSexLabBedSetting() const {
	return useSexLabBedSetting;
}

void RndSexConfig::setUseSexLabBedSetting(const bool value) {
	useSexLabBedSetting = value;
}

bool RndSexConfig::isCalmAfterSex() const {
	return calmAfterSex;
}

void RndSexConfig::setCalmAfterSex(const bool value) {
	calmAfterSex = value;
}

bool RndSexConfig::isPlayerOnly() const {
	return playerOnly;
}

void RndSexConfig::setPlayerOnly(const bool value) {
	playerOnly = value;
}

bool RndSexConfig::isNoTimeRestriction() const {
	return noTimeRestriction;
}

void RndSexConfig::setNoTimeRestriction(const bool value) {
	noTimeRestriction = value;
}

UInt32 RndSexConfig::getFollowerOptionIndex() const {
	return followerOptionIndex;
}

void RndSexConfig::setFollowerOptionIndex(const UInt32 value) {
	if (value < MIN_FOLLOWER_OPTION_INDEX) {
		followerOptionIndex = MIN_FOLLOWER_OPTION_INDEX;
	} else if (value > MAX_FOLLOWER_OPTION_INDEX) {
		followerOptionIndex = MAX_FOLLOWER_OPTION_INDEX;
	} else {
		followerOptionIndex = value;
	}
}

UInt32 RndSexConfig::getArousalMinimum() const {
	return arousalMinimum;
}

void RndSexConfig::setArousalMinimum(const UInt32 value) {
	if (value < MIN_AROUSAL) {
		arousalMinimum = MIN_AROUSAL;
	} else if (value > MAX_AROUSAL) {
		arousalMinimum = MAX_AROUSAL;
	} else {
		arousalMinimum = value;
	}
}

UInt32 RndSexConfig::getMasturbatorMinimum() const {
	return masturbatorMinimum;
}

void RndSexConfig::setMasturbatorMinimum(const UInt32 value) {
	if (value < MIN_MASTURBATOR) {
		masturbatorMinimum = MIN_MASTURBATOR;
	} else if (value > MAX_MASTURBATOR) {
		masturbatorMinimum = MAX_MASTURBATOR;
	} else {
		masturbatorMinimum = value;
	}

	if (masturbatorMinimum > masturbatorMaximum) {
		masturbatorMaximum = masturbatorMinimum;
	}
}

UInt32 RndSexConfig::getMasturbatorMaximum() const {
	return masturbatorMaximum;
}

void RndSexConfig::setMasturbatorMaximum(const UInt32 value) {
	if (value < MIN_MASTURBATOR) {
		masturbatorMaximum = MIN_MASTURBATOR;
	} else if (value > MAX_MASTURBATOR) {
		masturbatorMaximum = MAX_MASTURBATOR;
	} else {
		masturbatorMaximum = value;
	}

	if (masturbatorMinimum > masturbatorMaximum) {
		UInt32 value = masturbatorMaximum;
		masturbatorMinimum = value;
	}
}

float RndSexConfig::getActivityStartHour() const {
	return activityStartHour;
}

void RndSexConfig::setActivityStartHour(const float value) {
	if (value < MIN_ACTIVITY_HOUR) {
		activityStartHour = MIN_ACTIVITY_HOUR;
	} else if (value > MAX_ACTIVITY_HOUR) {
		activityStartHour = MAX_ACTIVITY_HOUR;
	} else {
		activityStartHour = value;
	}
}

float RndSexConfig::getActivityEndHour() const {
	return activityEndHour;
}

void RndSexConfig::setActivityEndHour(const float value) {
	if (value < MIN_ACTIVITY_HOUR) {
		activityEndHour = MIN_ACTIVITY_HOUR;
	} else if (value > MAX_ACTIVITY_HOUR) {
		activityEndHour = MAX_ACTIVITY_HOUR;
	} else {
		activityEndHour = value;
	}
}

float RndSexConfig::getMaxDistanceMeters() const {
	return maxDistanceMeters;
}

void RndSexConfig::setMaxDistanceMeters(const float value) {
	if (value < MIN_DISTANCE_METERS) {
		maxDistanceMeters = MIN_DISTANCE_METERS;
	} else if (value > MAX_DISTANCE_METERS) {
		maxDistanceMeters = MAX_DISTANCE_METERS;
	} else {
		maxDistanceMeters = value;
	}
}

float RndSexConfig::getPercentCreatureSex() const {
	return percentCreatureSex;
}

void RndSexConfig::setPercentCreatureSex(const float value) {
	if (value < MIN_PERCENTAGE) {
		percentCreatureSex = MIN_PERCENTAGE;
	} else if (value > MAX_PERCENTAGE) {
		percentCreatureSex = MAX_PERCENTAGE;
	} else {
		percentCreatureSex = value;
	}
}

float RndSexConfig::getPercentGroupSex() const {
	return percentGroupSex;
}

void RndSexConfig::setPercentGroupSex(const float value) {
	if (value < MIN_PERCENTAGE) {
		percentGroupSex = MIN_PERCENTAGE;
	} else if (value > MAX_PERCENTAGE) {
		percentGroupSex = MAX_PERCENTAGE;
	} else {
		percentGroupSex = value;
	}
}

float RndSexConfig::getPercentCoupleSex() const {
	return percentCoupleSex;
}

void RndSexConfig::setPercentCoupleSex(const float value) {
	if (value < MIN_PERCENTAGE) {
		percentCoupleSex = MIN_PERCENTAGE;
	} else if (value > MAX_PERCENTAGE) {
		percentCoupleSex = MAX_PERCENTAGE;
	} else {
		percentCoupleSex = value;
	}
}

float RndSexConfig::getPercentMasturbator() const {
	return percentMasturbator;
}

void RndSexConfig::setPercentMasturbator(const float value) {
	if (value < MIN_PERCENTAGE) {
		percentMasturbator = MIN_PERCENTAGE;
	} else if (value > MAX_PERCENTAGE) {
		percentMasturbator = MAX_PERCENTAGE;
	} else {
		percentMasturbator = value;
	}
}

float RndSexConfig::getPercentCreatureOnPlayer() const {
	return percentCreatureOnPlayer;
}

void RndSexConfig::setPercentCreatureOnPlayer(const float value) {
	if (value < MIN_PERCENTAGE) {
		percentCreatureOnPlayer = MIN_PERCENTAGE;
	} else if (value > MAX_PERCENTAGE) {
		percentCreatureOnPlayer = MAX_PERCENTAGE;
	} else {
		percentCreatureOnPlayer = value;
	}
}

float RndSexConfig::getPercentNpcOnPlayer() const {
	return percentNpcOnPlayer;
}

void RndSexConfig::setPercentNpcOnPlayer(const float value) {
	if (value < MIN_PERCENTAGE) {
		percentNpcOnPlayer = MIN_PERCENTAGE;
	} else if (value > MAX_PERCENTAGE) {
		percentNpcOnPlayer = MAX_PERCENTAGE;
	} else {
		percentNpcOnPlayer = value;
	}
}

float RndSexConfig::getIntervalMinimum() const {
	return intervalMinimumMinutes * 60.0f + intervalMinimumSeconds;
}

float RndSexConfig::getIntervalMinimumMinutes() const {
	return intervalMinimumMinutes;
}

void RndSexConfig::setIntervalMinimumMinutes(const float value) {
	if (value < MIN_INTERVAL_MINUTE) {
		intervalMinimumMinutes = MIN_INTERVAL_MINUTE;
	} else if (value > MAX_INTERVAL_MINUTE) {
		intervalMinimumMinutes = MAX_INTERVAL_MINUTE;
	} else {
		intervalMinimumMinutes = value;
	}

	validateMinimumInterval();
}

float RndSexConfig::getIntervalMinimumSeconds() const {
	return intervalMinimumSeconds;
}

void RndSexConfig::setIntervalMinimumSeconds(const float value) {
	if (value < MIN_INTERVAL_SECOND) {
		intervalMinimumSeconds = MIN_INTERVAL_SECOND;
	} else if (value > MAX_INTERVAL_SECOND) {
		intervalMinimumSeconds = MAX_INTERVAL_SECOND;
	} else {
		intervalMinimumSeconds = value;
	}

	validateMinimumInterval();
}

void RndSexConfig::validateMinimumInterval() {
	float intervalMinimum = intervalMinimumMinutes * 60.0f + intervalMinimumSeconds;
	float intervalMaximum = intervalMaximumMinutes * 60.0f + intervalMaximumSeconds;

	if (intervalMinimum < MIN_INTERVAL) {
		intervalMinimum = MIN_INTERVAL;
		intervalMinimumMinutes = MIN_INTERVAL_MINUTE;
		intervalMinimumSeconds = MIN_INTERVAL;
	}

	if (intervalMinimum > intervalMaximum) {
		intervalMaximumMinutes = intervalMinimumMinutes;
		intervalMaximumSeconds = intervalMinimumSeconds;
	}
}

float RndSexConfig::getIntervalMaximum() const {
	return intervalMaximumMinutes * 60.0F + intervalMaximumSeconds;
}

float RndSexConfig::getIntervalMaximumMinutes() {
	return intervalMaximumMinutes;
}

void RndSexConfig::setIntervalMaximumMinutes(const float value) {
	if (value < MIN_INTERVAL_MINUTE) {
		intervalMaximumMinutes = MIN_INTERVAL_MINUTE;
	} else if (value > MAX_INTERVAL_MINUTE) {
		intervalMaximumMinutes = MAX_INTERVAL_MINUTE;
	} else {
		intervalMaximumMinutes = value;
	}

	validateMaximumInterval();
}

float RndSexConfig::getIntervalMaximumSeconds() const {
	return intervalMaximumSeconds;
}

void RndSexConfig::setIntervalMaximumSeconds(const float value) {
	if (value < MIN_INTERVAL_SECOND) {
		intervalMaximumSeconds = MIN_INTERVAL_SECOND;
	} else if (value > MAX_INTERVAL_SECOND) {
		intervalMaximumSeconds = MAX_INTERVAL_SECOND;
	} else {
		intervalMaximumSeconds = value;
	}

	validateMaximumInterval();
}

void RndSexConfig::validateMaximumInterval() {
	float intervalMinimum = intervalMinimumMinutes * 60.0f + intervalMinimumSeconds;
	float intervalMaximum = intervalMaximumMinutes * 60.0f + intervalMaximumSeconds;

	if (intervalMaximum < MIN_INTERVAL) {
		intervalMaximum = MIN_INTERVAL;
		intervalMaximumMinutes = MIN_INTERVAL_MINUTE;
		intervalMaximumSeconds = MIN_INTERVAL;
	}

	if (intervalMinimum > intervalMaximum) {
		intervalMinimumMinutes = intervalMaximumMinutes;
		intervalMinimumSeconds = intervalMaximumSeconds;
	}
}

bool RndSexConfig::isCreatureSexAvailable() const {
	return ((percentCreatureSex > 0.0F) && (sexActFlags.isCreatureMale() || sexActFlags.isCreatureFemale()));
}

bool RndSexConfig::isGroupSexAvailable() const {
	return ((percentGroupSex > 0.0F) && (sexActFlags.isGroupAllMale() || sexActFlags.isGroupOneFemale() || sexActFlags.isGroupMixed() || sexActFlags.isGroupOneMale() || sexActFlags.isGroupAllFemale()));
}

bool RndSexConfig::isCoupleSexAvailable() const {
	return ((percentCoupleSex > 0.0F) && (sexActFlags.isCoupleAllMale() || sexActFlags.isCoupleMixed() || sexActFlags.isCoupleAllFemale()));
}

bool RndSexConfig::isSoloSexAvailable() const {
	return (sexActFlags.isSoloMale() || sexActFlags.isSoloFemale());
}

RndSexConfigFlags &RndSexConfig::getSexActFlags() {
	return sexActFlags;
}

RndSexConfigFlags &RndSexConfig::getMaleMasturbatorFlags() {
	return maleMasturbatorFlags;
}

RndSexConfigFlags &RndSexConfig::getFemaleMasturbatorFlags() {
	return femaleMasturbatorFlags;
}

void RndSexConfig::load(SKSESerializationInterface *serializer, UInt32 version) {
	serializer->ReadRecordData(&questActive, sizeof(questActive));
	serializer->ReadRecordData(&showNotifications, sizeof(showNotifications));
	serializer->ReadRecordData(&allowOldPeople, sizeof(allowOldPeople));
	serializer->ReadRecordData(&maleInMalePosition, sizeof(maleInMalePosition));
	serializer->ReadRecordData(&useSexLabBedSetting, sizeof(useSexLabBedSetting));
	serializer->ReadRecordData(&calmAfterSex, sizeof(calmAfterSex));
	serializer->ReadRecordData(&playerOnly, sizeof(playerOnly));
	serializer->ReadRecordData(&allowDragons, sizeof(allowDragons));
	serializer->ReadRecordData(&noTimeRestriction, sizeof(noTimeRestriction));
	serializer->ReadRecordData(&followerOptionIndex, sizeof(followerOptionIndex));
	serializer->ReadRecordData(&arousalMinimum, sizeof(arousalMinimum));
	serializer->ReadRecordData(&activityStartHour, sizeof(activityStartHour));
	serializer->ReadRecordData(&activityEndHour, sizeof(activityEndHour));
	serializer->ReadRecordData(&maxDistanceMeters, sizeof(maxDistanceMeters));
	serializer->ReadRecordData(&intervalMinimumMinutes, sizeof(intervalMinimumMinutes));
	serializer->ReadRecordData(&intervalMinimumSeconds, sizeof(intervalMinimumSeconds));
	serializer->ReadRecordData(&intervalMaximumMinutes, sizeof(intervalMaximumMinutes));
	serializer->ReadRecordData(&intervalMaximumSeconds, sizeof(intervalMaximumSeconds));
	serializer->ReadRecordData(&masturbatorMinimum, sizeof(masturbatorMinimum));
	serializer->ReadRecordData(&masturbatorMaximum, sizeof(masturbatorMaximum));
	serializer->ReadRecordData(&percentCreatureSex, sizeof(percentCreatureSex));
	serializer->ReadRecordData(&percentGroupSex, sizeof(percentGroupSex));
	serializer->ReadRecordData(&percentCoupleSex, sizeof(percentCoupleSex));
	serializer->ReadRecordData(&percentMasturbator, sizeof(percentMasturbator));
	serializer->ReadRecordData(&percentCreatureOnPlayer, sizeof(percentCreatureOnPlayer));
	serializer->ReadRecordData(&percentNpcOnPlayer, sizeof(percentNpcOnPlayer));

	if (version > 1) {
		serializer->ReadRecordData(&allowBeastRaces, sizeof(allowBeastRaces));
	}

	sexActFlags.load(serializer, version);
	maleMasturbatorFlags.load(serializer, version);
	femaleMasturbatorFlags.load(serializer, version);
}

void RndSexConfig::save(SKSESerializationInterface *serializer) const {
	serializer->WriteRecordData(&questActive, sizeof(questActive));
	serializer->WriteRecordData(&showNotifications, sizeof(showNotifications));
	serializer->WriteRecordData(&allowOldPeople, sizeof(allowOldPeople));
	serializer->WriteRecordData(&maleInMalePosition, sizeof(maleInMalePosition));
	serializer->WriteRecordData(&useSexLabBedSetting, sizeof(useSexLabBedSetting));
	serializer->WriteRecordData(&calmAfterSex, sizeof(calmAfterSex));
	serializer->WriteRecordData(&playerOnly, sizeof(playerOnly));
	serializer->WriteRecordData(&allowDragons, sizeof(allowDragons));
	serializer->WriteRecordData(&noTimeRestriction, sizeof(noTimeRestriction));
	serializer->WriteRecordData(&followerOptionIndex, sizeof(followerOptionIndex));
	serializer->WriteRecordData(&arousalMinimum, sizeof(arousalMinimum));
	serializer->WriteRecordData(&activityStartHour, sizeof(activityStartHour));
	serializer->WriteRecordData(&activityEndHour, sizeof(activityEndHour));
	serializer->WriteRecordData(&maxDistanceMeters, sizeof(maxDistanceMeters));
	serializer->WriteRecordData(&intervalMinimumMinutes, sizeof(intervalMinimumMinutes));
	serializer->WriteRecordData(&intervalMinimumSeconds, sizeof(intervalMinimumSeconds));
	serializer->WriteRecordData(&intervalMaximumMinutes, sizeof(intervalMaximumMinutes));
	serializer->WriteRecordData(&intervalMaximumSeconds, sizeof(intervalMaximumSeconds));
	serializer->WriteRecordData(&masturbatorMinimum, sizeof(masturbatorMinimum));
	serializer->WriteRecordData(&masturbatorMaximum, sizeof(masturbatorMaximum));
	serializer->WriteRecordData(&percentCreatureSex, sizeof(percentCreatureSex));
	serializer->WriteRecordData(&percentGroupSex, sizeof(percentGroupSex));
	serializer->WriteRecordData(&percentCoupleSex, sizeof(percentCoupleSex));
	serializer->WriteRecordData(&percentMasturbator, sizeof(percentMasturbator));
	serializer->WriteRecordData(&percentCreatureOnPlayer, sizeof(percentCreatureOnPlayer));
	serializer->WriteRecordData(&percentNpcOnPlayer, sizeof(percentNpcOnPlayer));
	serializer->WriteRecordData(&allowBeastRaces, sizeof(allowBeastRaces));

	sexActFlags.save(serializer);
	maleMasturbatorFlags.save(serializer);
	femaleMasturbatorFlags.save(serializer);
}
