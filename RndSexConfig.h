#pragma once

#include <atomic>
#include <shared_mutex>
#include "common/ITypes.h"
#include "RndSexConfigFlags.h"

class RndSexConfig {
public:
	RndSexConfig();

	bool isQuestActive() const;
	void setQuestActive(const bool value);
	bool isShowNotifications() const;
	void setShowNotifications(const bool value);
	bool isAllowOldPeople() const;
	void setAllowOldPeople(const bool value);
	bool isAllowDragons() const;
	void setAllowDragons(const bool value);
	bool isAllowBeastRaces() const;
	void setAllowBeastRaces(const bool value);
	bool isMaleInMalePosition() const;
	void setMaleInMalePosition(const bool value);
	bool isUseSexLabBedSetting() const;
	void setUseSexLabBedSetting(const bool value);
	bool isCalmAfterSex() const;
	void setCalmAfterSex(const bool value);
	bool isPlayerOnly() const;
	void setPlayerOnly(const bool value);
	bool isNoTimeRestriction() const;
	void setNoTimeRestriction(const bool value);

	UInt32 getFollowerOptionIndex() const;
	void setFollowerOptionIndex(const UInt32 value);
	UInt32 getArousalMinimum() const;
	void setArousalMinimum(const UInt32 value);
	UInt32 getMasturbatorMinimum() const;
	void setMasturbatorMinimum(const UInt32 value);
	UInt32 getMasturbatorMaximum() const;
	void setMasturbatorMaximum(const UInt32 value);

	float getActivityStartHour() const;
	void setActivityStartHour(const float value);
	float getActivityEndHour() const;
	void setActivityEndHour(const float value);
	float getMaxDistanceMeters() const;
	void setMaxDistanceMeters(const float value);
	float getPercentCreatureSex() const;
	void setPercentCreatureSex(const float value);
	float getPercentGroupSex() const;
	void setPercentGroupSex(const float value);
	float getPercentCoupleSex() const;
	void setPercentCoupleSex(const float value);
	float getPercentMasturbator() const;
	void setPercentMasturbator(const float value);
	float getPercentCreatureOnPlayer() const;
	void setPercentCreatureOnPlayer(const float value);
	float getPercentNpcOnPlayer() const;
	void setPercentNpcOnPlayer(const float value);

	float getIntervalMinimum() const;
	float getIntervalMinimumMinutes() const;
	void setIntervalMinimumMinutes(const float value);
	float getIntervalMinimumSeconds() const;
	void setIntervalMinimumSeconds(const float value);
	float getIntervalMaximum() const;
	float getIntervalMaximumMinutes();
	void setIntervalMaximumMinutes(const float value);
	float getIntervalMaximumSeconds() const;
	void setIntervalMaximumSeconds(const float value);

	bool isCreatureSexAvailable() const;
	bool isGroupSexAvailable() const;
	bool isCoupleSexAvailable() const;
	bool isSoloSexAvailable() const;

	RndSexConfigFlags &getSexActFlags();
	RndSexConfigFlags &getMaleMasturbatorFlags();
	RndSexConfigFlags &getFemaleMasturbatorFlags();

	void load(SKSESerializationInterface *serializer, UInt32 version);
	void save(SKSESerializationInterface *serializer) const;
private:
	static constexpr UInt32 MIN_FOLLOWER_OPTION_INDEX = 0;
	static constexpr UInt32 MAX_FOLLOWER_OPTION_INDEX = 2;
	static constexpr UInt32 MIN_AROUSAL = 0;
	static constexpr UInt32 MAX_AROUSAL = 100;
	static constexpr UInt32 MIN_MASTURBATOR = 1;
	static constexpr UInt32 MAX_MASTURBATOR = 4;

	static constexpr float MIN_ACTIVITY_HOUR = 0.0F;
	static constexpr float MAX_ACTIVITY_HOUR = 23.0F;
	static constexpr float MIN_DISTANCE_METERS = 10.0F;
	static constexpr float MAX_DISTANCE_METERS = 120.0F;
	static constexpr float DEFAULT_DISTANCE_METERS = 60.0F;
	static constexpr float MIN_INTERVAL_MINUTE = 0.0F;
	static constexpr float MAX_INTERVAL_MINUTE = 90.0F;
	static constexpr float DEFAULT_INTERVAL_MINUTE = 1.0F;
	static constexpr float MIN_INTERVAL = 10.0F;
	static constexpr float MIN_INTERVAL_SECOND = 0.0F;
	static constexpr float MAX_INTERVAL_SECOND = 59.0F;
	static constexpr float MIN_PERCENTAGE = 0.0F;
	static constexpr float MAX_PERCENTAGE = 100.0F;
	static constexpr float DEFAULT_PERCENTAGE = 25.0F;

	bool questActive;
	bool showNotifications;
	bool allowOldPeople;
	bool maleInMalePosition;
	bool allowBeastRaces;
	bool useSexLabBedSetting;
	bool calmAfterSex;
	bool playerOnly;
	bool allowDragons;
	bool noTimeRestriction;

	UInt32 followerOptionIndex;
	UInt32 arousalMinimum;
	UInt32 masturbatorMinimum;
	UInt32 masturbatorMaximum;

	float activityStartHour;
	float activityEndHour;
	float maxDistanceMeters;
	float percentCreatureSex;
	float percentGroupSex;
	float percentCoupleSex;
	float percentMasturbator;
	float percentCreatureOnPlayer;
	float percentNpcOnPlayer;
	float intervalMinimumMinutes;
	float intervalMinimumSeconds;
	float intervalMaximumMinutes;
	float intervalMaximumSeconds;

	RndSexConfigFlags sexActFlags;
	RndSexConfigFlags maleMasturbatorFlags;
	RndSexConfigFlags femaleMasturbatorFlags;

	void validateMinimumInterval();
	void validateMaximumInterval();
};
