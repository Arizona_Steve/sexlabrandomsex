#pragma warning(disable: 4244)

#include "RndSexNpcAnimCounts.h"

// Constructor.
RndSexNpcAnimCounts::RndSexNpcAnimCounts() :
	oneMaleCount(0),
	oneFemaleCount(0),
	twoNpcCount(0),
	threeNpcCount(0),
	fourNpcCount(0),
	fiveNpcCount(0) {
}

// Reset all values to zero.
void RndSexNpcAnimCounts::clear() {
	oneMaleCount = 0;
	oneFemaleCount = 0;
	twoNpcCount = 0;
	threeNpcCount = 0;
	fourNpcCount = 0;
	fiveNpcCount = 0;
}

// Increment the number of animations involving one male NPC.
void RndSexNpcAnimCounts::incrementOneMaleCount() {
	oneMaleCount += 1;
}

// Increment the number of animations involving one female NPC.
void RndSexNpcAnimCounts::incrementOneFemaleCount() {
	oneFemaleCount += 1;
}

// Increment the number of animations involving two NPCs.
void RndSexNpcAnimCounts::incrementTwoNpcCount() {
	twoNpcCount += 1;
}

// Increment the number of animations involving three NPCs.
void RndSexNpcAnimCounts::incrementThreeNpcCount() {
	threeNpcCount += 1;
}

// Increment the number of animations involving four NPCs.
void RndSexNpcAnimCounts::incrementFourNpcCount() {
	fourNpcCount += 1;
}

// Increment the number of animations involving five NPCs.
void RndSexNpcAnimCounts::incrementFiveNpcCount() {
	fiveNpcCount += 1;
}

// Return the number of animations involving one male NPC.
UInt32 RndSexNpcAnimCounts::getOneMaleCount() const {
	return oneMaleCount;
}

// Return the number of animations involving one female NPC.
UInt32 RndSexNpcAnimCounts::getOneFemaleCount() const {
	return oneFemaleCount;
}

// Return the number of animations involving two NPCs.
UInt32 RndSexNpcAnimCounts::getTwoNpcCount() const {
	return twoNpcCount;
}

// Return the number of animations involving three NPCs.
UInt32 RndSexNpcAnimCounts::getThreeNpcCount() const {
	return threeNpcCount;
}

// Return the number of animations involving four NPCs.
UInt32 RndSexNpcAnimCounts::getFourNpcCount() const {
	return fourNpcCount;
}

// Return the number of animations involving five NPCs.
UInt32 RndSexNpcAnimCounts::getFiveNpcCount() const {
	return fiveNpcCount;
}

// Return true if solo sex animations are available.
bool RndSexNpcAnimCounts::hasSoloMaleAnimations() const {
	return (oneMaleCount > 0);
}

// Return true if solo sex animations are available.
bool RndSexNpcAnimCounts::hasSoloFemaleAnimations() const {
	return (oneFemaleCount > 0);
}

// Return true if couple sex animations are available.
bool RndSexNpcAnimCounts::hasCoupleAnimations() const {
	return (twoNpcCount > 0);
}

// Return true if group sex animations are available.
bool RndSexNpcAnimCounts::hasGroupAnimations() const {
	return ((threeNpcCount > 0) || (fourNpcCount > 0) || (fiveNpcCount > 0));
}
