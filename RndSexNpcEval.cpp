#pragma warning(disable: 4244)

#include <random>
#include "windows.h"
#include "RndSexConfig.h"
#include "RndSexNpcAnim.h"
#include "RndSexNpcEval.h"

// Defined in RndSexConfigWrapper.cpp
extern RndSexConfig config;

// Defined in RndSexNpcAnimWrapper.cpp
extern RndSexNpcAnim npcAnimations;

// Defined in RndSexEvaluator.cpp
extern std::default_random_engine random;
extern std::uniform_real_distribution<float> percentDistribution;

// Clears out any entries from a previous evaluation.
void RndSexNpcEval::clear() {
	attackerEvaluator.clear();
	victimEvaluator.clear();
}

// Adds a potential male attacker to the male attacker NPC list.
void RndSexNpcEval::addMaleAttacker(Actor *actor) {
	attackerEvaluator.addMale(actor);
}

// Adds a potential male victim to the male victim NPC list.
void RndSexNpcEval::addMaleVictim(Actor *actor) {
	victimEvaluator.addMale(actor);
}

// Adds a potential female attacker to the female attacker NPC list.
void RndSexNpcEval::addFemaleAttacker(Actor *actor) {
	attackerEvaluator.addFemale(actor);
}

// Adds a potential female victim to the female victim NPC list.
void RndSexNpcEval::addFemaleVictim(Actor *actor) {
	victimEvaluator.addFemale(actor);
}

// Returns the total number of male NPCs.
size_t RndSexNpcEval::getMaleCount() const {
	return attackerEvaluator.getMaleCount() + victimEvaluator.getMaleCount();
}

// Returns the total number of female NPCs.
size_t RndSexNpcEval::getFemaleCount() const {
	return attackerEvaluator.getFemaleCount() + victimEvaluator.getFemaleCount();
}

// Checks configuration parameters to determine if masturbating onlookers should
// be selected for a sex act involving creatures. If so, the configuration
// determines the number of masterbating onlookers that are selected.
void RndSexNpcEval::selectMasturbatorsForCreatureSex(VMResultArray<Actor *> &sexActorArray, const bool victimFemale) {
	attackerEvaluator.selectMasturbatorsForCreatureSex(sexActorArray, victimFemale);
}

// Evaluate a potential group sex act.
bool RndSexNpcEval::evaluateGroupSex(VMResultArray<Actor*> &sexActorArray, Actor *player, const bool playerFemale) {
	if (config.isGroupSexAvailable() && npcAnimations.hasGroupAnimations() && ((!config.isCoupleSexAvailable() && !config.isSoloSexAvailable()) || (percentDistribution(random) < config.getPercentGroupSex()))) {
		try {
			bool victimFemale = false;
			Actor *victim = selectVictimForGroupSex(player, playerFemale, victimFemale);
			UInt32 npcCount = attackerEvaluator.determineGroupSexCount();
			UInt32 femaleCount = 0;
			attackerEvaluator.selectAttackersForGroupSex(sexActorArray, victim, victimFemale, npcCount, femaleCount);
			attackerEvaluator.selectMasturbatorsForGroupSex(sexActorArray, npcCount, femaleCount);
			return true;
		} catch (...) {
			sexActorArray.resize(0, nullptr);
			restoreLists();
		}
	}

	return false;
}

// Evaluate a potential couple sex act.
bool RndSexNpcEval::evaluateCoupleSex(VMResultArray<Actor *> &sexActorArray, Actor *player, const bool playerFemale) {
	if (config.isCoupleSexAvailable() && npcAnimations.hasCoupleAnimations() && (!config.isSoloSexAvailable() || (percentDistribution(random) < config.getPercentCoupleSex()))) {
		try {
			bool victimFemale = false;
			Actor *victim = selectVictimForCoupleSex(player, playerFemale, victimFemale);
			UInt32 femaleCount = 0;
			attackerEvaluator.selectAttackerForCoupleSex(sexActorArray, victim, victimFemale, femaleCount);
			attackerEvaluator.selectMasturbatorsForCoupleSex(sexActorArray, femaleCount);
			return true;
		} catch (...) {
			sexActorArray.resize(0, nullptr);
			restoreLists();
		}
	}

	return false;
}

// Evaluate a solo masturbator sex act.
void RndSexNpcEval::evaluateSoloSex(VMResultArray<Actor *> &sexActorArray) {
	if (!config.isPlayerOnly() && config.isSoloSexAvailable()) {
		try {
			RndSexConfigFlags &sexActFlags = config.getSexActFlags();
			bool masturbatorFemale = false;
			bool selectMale = (npcAnimations.hasSoloMaleAnimations() && sexActFlags.isSoloMale());
			bool selectFemale = (npcAnimations.hasSoloFemaleAnimations() && sexActFlags.isSoloFemale());
			Actor *masturbator = selectNpc(selectMale, selectFemale, false, false, masturbatorFemale);
			sexActorArray.resize(1, masturbator);
			attackerEvaluator.selectMasturbatorsForSoloSex(sexActorArray, masturbatorFemale);
		} catch (...) {
			sexActorArray.resize(0, nullptr);
		}
	}
}

// Selects a victim at random from either the NPC attacker or victim lists,
// dependent only on whether male and/or female NPCs flags are specified.
// Updates the specified female victim flag to true if the selected victim is
// female or to false if the selected victim is male.
Actor *RndSexNpcEval::selectNpc(const bool maleAttacker, const bool femaleAttacker, const bool maleVictim, const bool femaleVictim, bool &victimFemale) {
	size_t victimCount = (maleVictim) ? victimEvaluator.getMaleCount() : 0;

	if (femaleVictim) {
		victimCount += victimEvaluator.getFemaleCount();
	}

	size_t totalCount = victimCount;

	if (maleAttacker) {
		totalCount += attackerEvaluator.getMaleCount();
	}

	if (femaleAttacker) {
		totalCount += attackerEvaluator.getFemaleCount();
	}

	if (totalCount == 0) {
		throw std::out_of_range("RndSexNpcEval: No available NPCs");
	}

	size_t selectedIndex = 0;

	if (totalCount > 1) {
		std::uniform_int_distribution<size_t> distribution(0, totalCount - 1);
		selectedIndex = distribution(random);
	}

	if (selectedIndex < victimCount) {
		return victimEvaluator.selectNpc(maleVictim, femaleVictim, victimFemale);
	} else {
		return attackerEvaluator.selectNpc(maleAttacker, femaleAttacker, victimFemale);
	}
}

// Used by Papyrus to return the evaluated masturbator array.
VMResultArray<Actor*> RndSexNpcEval::getMasturbatorArray() {
	return attackerEvaluator.getMasturbatorArray();
}

// Copy back previously assigned NPCs and clear out assigned actor lists.
void RndSexNpcEval::restoreLists() {
	attackerEvaluator.restoreLists();
	victimEvaluator.restoreLists();
}

// Select a victim NPC for a group sex act based on the available NPCs.
Actor *RndSexNpcEval::selectVictimForGroupSex(Actor *player, const bool playerFemale, bool &victimFemale) {
	if ((player != nullptr) && config.isPlayerOnly()) {
		victimFemale = playerFemale;
		return player;
	}

	RndSexConfigFlags &sexActFlags = config.getSexActFlags();
	size_t minimumTotalCount = (sexActFlags.isGroupAllMale() || sexActFlags.isGroupOneFemale() || sexActFlags.isGroupOneMale() || sexActFlags.isGroupAllFemale()) ? 3 : 4;
	size_t minimumMaleCount = (sexActFlags.isGroupAllFemale() ? 0 : (sexActFlags.isGroupOneMale() ? 1 : ((sexActFlags.isGroupOneFemale() || sexActFlags.isGroupMixed()) ? 2 : 3)));
	size_t minimumFemaleCount = (sexActFlags.isGroupAllMale() ? 0 : (sexActFlags.isGroupOneFemale() ? 1 : ((sexActFlags.isGroupOneMale() || sexActFlags.isGroupMixed()) ? 2 : 3)));
	size_t maleAttackerCount = attackerEvaluator.getMaleCount();
	size_t femaleAttackerCount = attackerEvaluator.getFemaleCount();
	size_t totalAttackerCount = maleAttackerCount + femaleAttackerCount;

	if ((player != nullptr) && (percentDistribution(random) < config.getPercentNpcOnPlayer())) {
		bool hasAvailableMales = ((minimumMaleCount == 0) || (maleAttackerCount >= (playerFemale ? minimumMaleCount : (minimumMaleCount - 1))));
		bool hasAvailableFemales = ((minimumFemaleCount == 0) || (femaleAttackerCount >= (playerFemale ? (minimumFemaleCount - 1) : minimumFemaleCount)));

		if (hasAvailableMales && hasAvailableFemales && (totalAttackerCount >= (minimumTotalCount - 1))) {
			victimFemale = playerFemale;
			return player;
		}
	}

	bool selectVictimBoth = (sexActFlags.isGroupOneFemale() || sexActFlags.isGroupMixed() || sexActFlags.isGroupOneMale());
	bool selectVictimMale = (selectVictimBoth || sexActFlags.isGroupAllMale());
	bool selectVictimFemale = (selectVictimBoth || sexActFlags.isGroupAllFemale());
	bool selectAttackers = (totalAttackerCount >= minimumTotalCount);
	bool selectAttackerMale = (selectAttackers && selectVictimMale && (maleAttackerCount >= minimumMaleCount));
	bool selectAttackerFemale = (selectAttackers && selectVictimFemale && (femaleAttackerCount >= minimumFemaleCount));
	return selectNpc(selectAttackerMale, selectAttackerFemale, selectVictimMale, selectVictimFemale, victimFemale);
}

// Select a victim NPC for a couple sex act based on the available NPCs.
Actor * RndSexNpcEval::selectVictimForCoupleSex(Actor * player, const bool playerFemale, bool & victimFemale) {
	if ((player != nullptr) && config.isPlayerOnly()) {
		victimFemale = playerFemale;
		return player;
	}

	RndSexConfigFlags &sexActFlags = config.getSexActFlags();
	size_t minimumMaleCount = (sexActFlags.isCoupleAllFemale() ? 0 : (sexActFlags.isCoupleMixed() ? 1 : 2));
	size_t minimumFemaleCount = (sexActFlags.isCoupleAllMale() ? 0 : (sexActFlags.isCoupleMixed() ? 1 : 2));
	size_t maleAttackerCount = attackerEvaluator.getMaleCount();
	size_t femaleAttackerCount = attackerEvaluator.getFemaleCount();
	size_t totalAttackerCount = maleAttackerCount + femaleAttackerCount;

	if ((player != nullptr) && (percentDistribution(random) < config.getPercentNpcOnPlayer())) {
		bool hasAvailableMales = ((minimumMaleCount == 0) || (maleAttackerCount >= (playerFemale ? minimumMaleCount : (minimumMaleCount - 1))));
		bool hasAvailableFemales = ((minimumFemaleCount == 0) || (femaleAttackerCount >= (playerFemale ? (minimumFemaleCount - 1) : minimumFemaleCount)));

		if (hasAvailableMales && hasAvailableFemales && (totalAttackerCount >= 1)) {
			victimFemale = playerFemale;
			return player;
		}
	}

	bool selectVictimBoth = sexActFlags.isCoupleMixed();
	bool selectVictimMale = (selectVictimBoth || sexActFlags.isCoupleAllMale());
	bool selectVictimFemale = (selectVictimBoth || sexActFlags.isCoupleAllFemale());
	bool selectAttackers = (totalAttackerCount >= 2);
	bool selectAttackerMale = (selectAttackers && selectVictimMale && (maleAttackerCount >= minimumMaleCount));
	bool selectAttackerFemale = (selectAttackers && selectVictimFemale && (femaleAttackerCount >= minimumFemaleCount));
	return selectNpc(selectAttackerMale, selectAttackerFemale, selectVictimMale, selectVictimFemale, victimFemale);
}
