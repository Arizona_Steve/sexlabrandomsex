#pragma once

#include <shared_mutex>
#include "RndSexNpcAnimCounts.h"

class RndSexNpcAnim {
public:
	RndSexNpcAnim();
	~RndSexNpcAnim();

	void beginRefresh();
	void addEntry(UInt32 maleCount, UInt32 femaleCount);
	void completeRefresh();
	UInt32 getOneMaleCount() const;
	UInt32 getOneFemaleCount() const;
	UInt32 getTwoNpcCount() const;
	UInt32 getThreeNpcCount() const;
	UInt32 getFourNpcCount() const;
	UInt32 getFiveNpcCount() const;
	bool hasSoloMaleAnimations() const;
	bool hasSoloFemaleAnimations() const;
	bool hasCoupleAnimations() const;
	bool hasGroupAnimations() const;
private:
	mutable std::shared_mutex mainMutex;
	mutable std::mutex tempMutex;
	RndSexNpcAnimCounts *mainAnimationCounts;
	RndSexNpcAnimCounts *tempAnimationCounts;
};
