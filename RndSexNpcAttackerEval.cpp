#pragma warning(disable: 4244)

#include <random>
#include "windows.h"
#include "RndSexConfig.h"
#include "RndSexNpcAnim.h"
#include "RndSexNpcAttackerEval.h"

// Defined in RndSexConfigWrapper.cpp
extern RndSexConfig config;

// Defined in RndSexNpcAnimWrapper.cpp
extern RndSexNpcAnim npcAnimations;

// Defined in RndSexEvaluator.cpp
extern std::default_random_engine random;
extern std::uniform_real_distribution<float> percentDistribution;

// Clears out the masturbator array in addition to superclass items.
void RndSexNpcAttackerEval::clear() {
	RndSexNpcVictimEval::clear();
	masturbatorArray.resize(0, nullptr);
}

// Determines the number of NPCs that will take part in a group sex act, based
// on the number of animations and the number of attackers available following
// victim selection.
UInt32 RndSexNpcAttackerEval::determineGroupSexCount() {
	size_t attackerCount = maleNpcList.getNpcCount() + femaleNpcList.getNpcCount();
	UInt32 threeNpcCount = 0;
	UInt32 fourNpcCount = 0;
	UInt32 animationCount = 0;

	if (attackerCount >= 2) {
		threeNpcCount = npcAnimations.getThreeNpcCount();
		animationCount = threeNpcCount;

		if (attackerCount >= 3) {
			fourNpcCount = npcAnimations.getFourNpcCount();
			animationCount += fourNpcCount;

			if (attackerCount >= 4) {
				animationCount += npcAnimations.getFiveNpcCount();
			}
		}
	}

	if (animationCount == 0) {
		throw std::out_of_range("RndSexNpcAttackerEval: No group sex animations available");
	}

	UInt32 selectedIndex = 0;

	if (animationCount > 1) {
		std::uniform_int_distribution<UInt32> distribution(0, animationCount - 1);
		selectedIndex = distribution(random);
	}

	if (selectedIndex < npcAnimations.getThreeNpcCount()) {
		return 3;
	} else {
		selectedIndex -= npcAnimations.getThreeNpcCount();
		return (selectedIndex < npcAnimations.getFourNpcCount()) ? 4 : 5;
	}
}

// Randomly select the appropriate number of attackers for a group sex act and
// update the sex actor array. Updates the specified female count with the
// number of female NPCs selected.
void RndSexNpcAttackerEval::selectAttackersForGroupSex(VMResultArray<Actor*> &sexActorArray, Actor *victim, const bool victimFemale, const UInt32 npcCount, UInt32 &femaleCount) {
	RndSexConfigFlags &sexActFlags = config.getSexActFlags();
	sexActorArray.resize(npcCount, nullptr);
	sexActorArray[0] = victim;
	bool selectMale = false;
	bool selectFemale = false;

	if (victimFemale) {
		femaleCount = 1;
		bool selectBoth = (sexActFlags.isGroupOneMale() || ((npcCount >= 4) && sexActFlags.isGroupMixed()));
		selectMale = (selectBoth || sexActFlags.isGroupOneFemale());
		selectFemale = (selectBoth || sexActFlags.isGroupAllFemale());
	} else {
		femaleCount = 0;
		bool selectBoth = (sexActFlags.isGroupOneFemale() || ((npcCount >= 4) && sexActFlags.isGroupMixed()));
		selectMale = (selectBoth || sexActFlags.isGroupAllMale());
		selectFemale = (selectBoth || sexActFlags.isGroupOneMale());
	}

	bool attackerFemale = false;
	sexActorArray[1] = selectNpc(selectMale, selectFemale, attackerFemale);

	if (attackerFemale) {
		++femaleCount;
	}

	if (femaleCount == 0) {
		selectMale = (sexActFlags.isGroupAllMale() || ((npcCount >= 4) && sexActFlags.isGroupOneFemale()) || ((npcCount >= 5) && sexActFlags.isGroupMixed()));
		selectFemale = (sexActFlags.isGroupOneFemale() || ((npcCount >= 4) && sexActFlags.isGroupMixed()));
	} else if (femaleCount == 1) {
		selectMale = (sexActFlags.isGroupOneFemale() || ((npcCount >= 4) && sexActFlags.isGroupMixed()));
		selectFemale = (sexActFlags.isGroupOneMale() || ((npcCount >= 4) && sexActFlags.isGroupMixed()));
	} else {
		selectMale = (sexActFlags.isGroupOneMale() || ((npcCount >= 4) && sexActFlags.isGroupMixed()));
		selectFemale = (sexActFlags.isGroupAllFemale() || ((npcCount >= 4) && sexActFlags.isGroupOneMale()) || ((npcCount >= 5) && sexActFlags.isGroupMixed()));
	}

	sexActorArray[2] = selectNpc(selectMale, selectFemale, attackerFemale);

	if (attackerFemale) {
		++femaleCount;
	}

	if (npcCount >= 4) {
		if (femaleCount == 0) {
			selectMale = (sexActFlags.isGroupAllMale() || ((npcCount >= 5) && sexActFlags.isGroupOneFemale()));
			selectFemale = (sexActFlags.isGroupOneFemale() || ((npcCount >= 5) && sexActFlags.isGroupMixed()));
		} else if (femaleCount == 1) {
			selectMale = (sexActFlags.isGroupOneFemale() || ((npcCount >= 5) && sexActFlags.isGroupMixed()));
			selectFemale = sexActFlags.isGroupMixed();
		} else if (femaleCount == 2) {
			selectMale = sexActFlags.isGroupMixed();
			selectFemale = (sexActFlags.isGroupOneMale() || ((npcCount >= 5) && sexActFlags.isGroupMixed()));
		} else {
			selectMale = (sexActFlags.isGroupOneMale() || ((npcCount >= 5) && sexActFlags.isGroupMixed()));
			selectFemale = (sexActFlags.isGroupAllFemale() || ((npcCount >= 5) && sexActFlags.isGroupOneMale()));
		}

		sexActorArray[3] = selectNpc(selectMale, selectFemale, attackerFemale);

		if (attackerFemale) {
			++femaleCount;
		}

		if (npcCount >= 5) {
			if (femaleCount == 0) {
				selectMale = sexActFlags.isGroupAllMale();
				selectFemale = sexActFlags.isGroupOneFemale();
			} else if (femaleCount == 1) {
				selectMale = sexActFlags.isGroupOneFemale();
				selectFemale = sexActFlags.isGroupMixed();
			} else if (femaleCount == 2) {
				selectMale = sexActFlags.isGroupMixed();
				selectFemale = sexActFlags.isGroupMixed();
			} else if (femaleCount == 3) {
				selectMale = sexActFlags.isGroupMixed();
				selectFemale = sexActFlags.isGroupOneMale();
			} else {
				selectMale = sexActFlags.isGroupOneMale();
				selectFemale = sexActFlags.isGroupAllFemale();
			}

			sexActorArray[4] = selectNpc(selectMale, selectFemale, attackerFemale);

			if (attackerFemale) {
				++femaleCount;
			}
		}
	}
}

// Randomly select an attacker for a couple sex act and update the sex actor
// array. Updates the specified female count with the number of female NPCs
// selected.
void RndSexNpcAttackerEval::selectAttackerForCoupleSex(VMResultArray<Actor *> &sexActorArray, Actor *victim, const bool victimFemale, UInt32 &femaleCount) {
	sexActorArray.resize(2, nullptr);
	sexActorArray[0] = victim;
	femaleCount = victimFemale ? 1 : 0;
	RndSexConfigFlags &sexActFlags = config.getSexActFlags();
	bool selectMale = victimFemale ? sexActFlags.isCoupleMixed() : sexActFlags.isCoupleAllMale();
	bool selectFemale = victimFemale ? sexActFlags.isCoupleAllFemale() : sexActFlags.isCoupleMixed();
	bool attackerFemale = false;
	sexActorArray[1] = selectNpc(selectMale, selectFemale, attackerFemale);

	if (attackerFemale) {
		++femaleCount;
	}
}

// Checks configuration parameters to determine if masturbating onlookers should
// be selected for a sex act involving creatures. If so, the configuration
// determines the number of masterbating onlookers that are selected.
void RndSexNpcAttackerEval::selectMasturbatorsForCreatureSex(VMResultArray<Actor *> &sexActorArray, const bool victimFemale) {
	if (sexActorArray.size() < 2) {
		sexActorArray.resize(0, nullptr);
		masturbatorArray.resize(0, nullptr);
	} else if (percentDistribution(random) < config.getPercentMasturbator()) {
		bool selectMale = (npcAnimations.hasSoloMaleAnimations() && (victimFemale ? config.getMaleMasturbatorFlags().isCreatureFemale() : config.getMaleMasturbatorFlags().isCreatureMale()));
		bool selectFemale = (npcAnimations.hasSoloFemaleAnimations() && (victimFemale ? config.getFemaleMasturbatorFlags().isCreatureFemale() : config.getFemaleMasturbatorFlags().isCreatureMale()));
		selectMasturbators(selectMale, selectFemale);
	}
}

// Checks configuration parameters to determine if masturbating onlookers should
// be selected for a group sex act. If so, the configuration determines the
// number of masterbating onlookers that are selected.
void RndSexNpcAttackerEval::selectMasturbatorsForGroupSex(VMResultArray<Actor *> &sexActorArray, const UInt32 npcCount, const UInt32 femaleCount) {
	if (sexActorArray.size() < 3) {
		sexActorArray.resize(0, nullptr);
		masturbatorArray.resize(0, nullptr);
	} else if (percentDistribution(random) < config.getPercentMasturbator()) {
		bool selectMale = false;
		bool selectFemale = false;

		if (femaleCount == 0) {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isGroupAllMale());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isGroupAllMale());
		} else if (femaleCount == 1) {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isGroupOneFemale());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isGroupOneFemale());
		} else if (femaleCount == npcCount) {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isGroupAllFemale());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isGroupAllFemale());
		} else if ((npcCount - femaleCount) == 1) {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isGroupOneMale());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isGroupOneMale());
		} else {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isGroupMixed());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isGroupMixed());
		}

		selectMasturbators(selectMale, selectFemale);
	}
}

// Selects masturbating onlookers for a couple sex act.
void RndSexNpcAttackerEval::selectMasturbatorsForCoupleSex(VMResultArray<Actor *> &sexActorArray, const UInt32 femaleCount) {
	if (sexActorArray.size() != 2) {
		sexActorArray.resize(0, nullptr);
		masturbatorArray.resize(0, nullptr);
	} else if (percentDistribution(random) < config.getPercentMasturbator()) {
		bool selectMale = false;
		bool selectFemale = false;

		if (femaleCount == 0) {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isCoupleAllMale());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isCoupleAllMale());
		} else if (femaleCount == 1) {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isCoupleMixed());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isCoupleMixed());
		} else {
			selectMale = (npcAnimations.hasSoloMaleAnimations() && config.getMaleMasturbatorFlags().isCoupleAllFemale());
			selectFemale = (npcAnimations.hasSoloFemaleAnimations() && config.getFemaleMasturbatorFlags().isCoupleAllFemale());
		}

		selectMasturbators(selectMale, selectFemale);
	}
}

// Selects masturbating onlookers for a solo sex act.
void RndSexNpcAttackerEval::selectMasturbatorsForSoloSex(VMResultArray<Actor *> &sexActorArray, const bool victimFemale) {
	if (sexActorArray.size() != 1) {
		sexActorArray.resize(0, nullptr);
		masturbatorArray.resize(0, nullptr);
	} else if (percentDistribution(random) < config.getPercentMasturbator()) {
		bool selectMale = (npcAnimations.hasSoloMaleAnimations() && (victimFemale ? config.getMaleMasturbatorFlags().isSoloFemale() : config.getMaleMasturbatorFlags().isSoloMale()));
		bool selectFemale = (npcAnimations.hasSoloFemaleAnimations() && (victimFemale ? config.getFemaleMasturbatorFlags().isSoloFemale() : config.getFemaleMasturbatorFlags().isSoloMale()));
		selectMasturbators(selectMale, selectFemale);
	}
}

// Selects masturbating onlookers from the attacker pool.
void RndSexNpcAttackerEval::selectMasturbators(const bool selectMale, const bool selectFemale) {
	UInt32 availableCount = (UInt32) ((selectMale ? maleNpcList.getNpcCount() : 0) + (selectFemale ? femaleNpcList.getNpcCount() : 0));

	if (availableCount < config.getMasturbatorMinimum()) {
		masturbatorArray.resize(0, nullptr);
	} else {
		UInt32 maximumMasturbators = (availableCount < config.getMasturbatorMaximum()) ? availableCount : config.getMasturbatorMaximum();
		UInt32 masturbatorCount = config.getMasturbatorMinimum();

		if (maximumMasturbators > config.getMasturbatorMinimum()) {
			std::uniform_int_distribution<UInt32> distribution(config.getMasturbatorMinimum(), maximumMasturbators);
			masturbatorCount = distribution(random);
		}

		bool dummyBool;
		masturbatorArray.resize(masturbatorCount, nullptr);

		try {
			for (UInt32 i = 0; i < masturbatorCount; ++i) {
				masturbatorArray[i] = selectNpc(selectMale, selectFemale, dummyBool);
			}
		} catch (...) {
			masturbatorArray.resize(0, nullptr);
		}
	}
}

// Used by Papyrus to return the evaluated masturbator array.
VMResultArray<Actor*> RndSexNpcAttackerEval::getMasturbatorArray() {
	return masturbatorArray;
}
