#pragma warning(disable: 4244)
#pragma warning(disable: 4267)

#include <stdio.h>
#include "windows.h"
#include "common/ITypes.h"
#include "common/IDebugLog.h"
#include "RndSexActivity.h"
#include "RndSexActivityWrapper.h"


namespace RndSexActivityWrapper {
	RndSexActivity activity;
	std::string message;

	BSFixedString getMessage(StaticFunctionTag* base, VMArray<BSFixedString> actorNameArray, const bool masturbatingOnlooker) {
		message = activity.getMessage(actorNameArray, masturbatingOnlooker);
		return BSFixedString(message.c_str());
	}

	bool registerFunctions(VMClassRegistry *registry) {
		registry->RegisterFunction(new NativeFunction2<StaticFunctionTag, BSFixedString, VMArray<BSFixedString>, bool>("getMessage", "RndSexActivityQuest", getMessage, registry));
		return true;
	}
}
