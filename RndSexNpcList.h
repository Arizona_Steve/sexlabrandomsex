#pragma once

#include <vector>
#include <shared_mutex>
#include "skse64/GameReferences.h"

typedef std::vector<Actor *> RndSexNpcActorList;

class RndSexNpcList {
public:
	void clear();
	void addNpc(Actor *actor);
	size_t getNpcCount() const;
	Actor *getNpcAtIndex(const size_t selectedIndex);
	void restoreLists();
private:
	RndSexNpcActorList currentNpcList;
	RndSexNpcActorList deletedNpcList;
};
