#pragma once

#include <shared_mutex>
#include "RndSexCreatureEval.h"
#include "RndSexNpcEval.h"

class RndSexEvaluator {
public:
	RndSexEvaluator();
	void clearEvaluator();
	void addEligiblePlayer(Actor *actor, bool actorFemale);
	void addMaleNpcAttacker(Actor *actor);
	void addMaleNpcVictim(Actor *actor);
	void addFemaleNpcAttacker(Actor *actor);
	void addFemaleNpcVictim(Actor *actor);
	void addCreature(BSFixedString &raceKey, Actor *actor);
	void evaluate(const bool creaturesAllowed);
	VMResultArray<Actor *> getSexActorArray();
	VMResultArray<Actor *> getMasturbatorArray();
private:
	mutable std::shared_mutex evaluatorMutex;
	Actor *player;
	bool playerFemale;
	bool usePlayer;
	RndSexCreatureEval creatureEvaluator;
	RndSexNpcEval npcEvaluator;
	VMResultArray<Actor *> sexActorArray;
	bool evaluateCreatureSex(const bool creaturesAllowed);
};
