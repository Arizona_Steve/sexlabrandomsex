#pragma warning(disable: 4244)

#include "windows.h"
#include "RndSexConfig.h"
#include "RndSexConfigWrapper.h"

RndSexConfig config;

namespace RndSexConfigWrapper {
	bool isQuestActive(StaticFunctionTag *base) {
		return config.isQuestActive();
	}

	void setQuestActive(StaticFunctionTag *base, const bool value) {
		config.setQuestActive(value);
	}

	bool isShowNotifications(StaticFunctionTag *base) {
		return config.isShowNotifications();
	}

	void setShowNotifications(StaticFunctionTag *base, const bool value) {
		config.setShowNotifications(value);
	}

	bool isAllowOldPeople(StaticFunctionTag *base) {
		return config.isAllowOldPeople();
	}

	void setAllowOldPeople(StaticFunctionTag *base, const bool value) {
		config.setAllowOldPeople(value);
	}

	bool isAllowDragons(StaticFunctionTag *base) {
		return config.isAllowDragons();
	}

	void setAllowDragons(StaticFunctionTag *base, const bool value) {
		config.setAllowDragons(value);
	}

	bool isAllowBeastRaces(StaticFunctionTag * base) {
		return config.isAllowBeastRaces();
	}

	void setAllowBeastRaces(StaticFunctionTag * base, const bool value) {
		config.setAllowBeastRaces(value);
	}

	bool isMaleInMalePosition(StaticFunctionTag *base) {
		return config.isMaleInMalePosition();
	}

	void setMaleInMalePosition(StaticFunctionTag *base, const bool value) {
		config.setMaleInMalePosition(value);
	}

	bool isUseSexLabBedSetting(StaticFunctionTag *base) {
		return config.isUseSexLabBedSetting();
	}

	void setUseSexLabBedSetting(StaticFunctionTag *base, const bool value) {
		config.setUseSexLabBedSetting(value);
	}

	bool isCalmAfterSex(StaticFunctionTag *base) {
		return config.isCalmAfterSex();
	}

	void setCalmAfterSex(StaticFunctionTag *base, const bool value) {
		config.setCalmAfterSex(value);
	}

	bool isPlayerOnly(StaticFunctionTag *base) {
		return config.isPlayerOnly();
	}
	void setPlayerOnly(StaticFunctionTag *base, const bool value) {
		config.setPlayerOnly(value);
	}

	bool isNoTimeRestriction(StaticFunctionTag *base) {
		return config.isNoTimeRestriction();
	}

	void setNoTimeRestriction(StaticFunctionTag *base, const bool value) {
		config.setNoTimeRestriction(value);
	}

	UInt32 getFollowerOptionIndex(StaticFunctionTag *base) {
		return config.getFollowerOptionIndex();
	}

	void setFollowerOptionIndex(StaticFunctionTag *base, const UInt32 value) {
		config.setFollowerOptionIndex(value);
	}

	UInt32 getArousalMinimum(StaticFunctionTag *base) {
		return config.getArousalMinimum();
	}

	void setArousalMinimum(StaticFunctionTag *base, const UInt32 value) {
		config.setArousalMinimum(value);
	}

	UInt32 getMasturbatorMinimum(StaticFunctionTag *base) {
		return config.getMasturbatorMinimum();
	}

	void setMasturbatorMinimum(StaticFunctionTag *base, const UInt32 value) {
		config.setMasturbatorMinimum(value);
	}

	UInt32 getMasturbatorMaximum(StaticFunctionTag *base) {
		return config.getMasturbatorMaximum();
	}

	void setMasturbatorMaximum(StaticFunctionTag *base, const UInt32 value) {
		config.setMasturbatorMaximum(value);
	}

	float getActivityStartHour(StaticFunctionTag *base) {
		return config.getActivityStartHour();
	}

	void setActivityStartHour(StaticFunctionTag *base, const float value) {
		config.setActivityStartHour(value);
	}

	float getActivityEndHour(StaticFunctionTag *base) {
		return config.getActivityEndHour();
	}

	void setActivityEndHour(StaticFunctionTag *base, const float value) {
		config.setActivityEndHour(value);
	}

	float getMaxDistanceMeters(StaticFunctionTag *base) {
		return config.getMaxDistanceMeters();
	}

	void setMaxDistanceMeters(StaticFunctionTag *base, const float value) {
		config.setMaxDistanceMeters(value);
	}

	float getPercentCreatureSex(StaticFunctionTag *base) {
		return config.getPercentCreatureSex();
	}

	void setPercentCreatureSex(StaticFunctionTag *base, const float value) {
		config.setPercentCreatureSex(value);
	}

	float getPercentGroupSex(StaticFunctionTag *base) {
		return config.getPercentGroupSex();
	}

	void setPercentGroupSex(StaticFunctionTag *base, const float value) {
		config.setPercentGroupSex(value);
	}

	float getPercentCoupleSex(StaticFunctionTag *base) {
		return config.getPercentCoupleSex();
	}

	void setPercentCoupleSex(StaticFunctionTag *base, const float value) {
		config.setPercentCoupleSex(value);
	}

	float getPercentMasturbator(StaticFunctionTag *base) {
		return config.getPercentMasturbator();
	}

	void setPercentMasturbator(StaticFunctionTag *base, const float value) {
		config.setPercentMasturbator(value);
	}

	float getPercentCreatureOnPlayer(StaticFunctionTag *base) {
		return config.getPercentCreatureOnPlayer();
	}

	void setPercentCreatureOnPlayer(StaticFunctionTag *base, const float value) {
		config.setPercentCreatureOnPlayer(value);
	}

	float getPercentNpcOnPlayer(StaticFunctionTag *base) {
		return config.getPercentNpcOnPlayer();
	}

	void setPercentNpcOnPlayer(StaticFunctionTag *base, const float value) {
		config.setPercentNpcOnPlayer(value);
	}

	float getIntervalMinimum(StaticFunctionTag *base) {
		return config.getIntervalMinimum();
	}

	float getIntervalMinimumMinutes(StaticFunctionTag *base) {
		return config.getIntervalMinimumMinutes();
	}

	void setIntervalMinimumMinutes(StaticFunctionTag *base, const float value) {
		config.setIntervalMinimumMinutes(value);
	}

	float getIntervalMinimumSeconds(StaticFunctionTag *base) {
		return config.getIntervalMinimumSeconds();
	}

	void setIntervalMinimumSeconds(StaticFunctionTag *base, const float value) {
		config.setIntervalMinimumSeconds(value);
	}

	float getIntervalMaximum(StaticFunctionTag *base) {
		return config.getIntervalMaximum();
	}

	float getIntervalMaximumMinutes(StaticFunctionTag *base) {
		return config.getIntervalMaximumMinutes();
	}

	void setIntervalMaximumMinutes(StaticFunctionTag *base, const float value) {
		config.setIntervalMaximumMinutes(value);
	}

	float getIntervalMaximumSeconds(StaticFunctionTag *base) {
		return config.getIntervalMaximumSeconds();
	}

	void setIntervalMaximumSeconds(StaticFunctionTag *base, const float value) {
		config.setIntervalMaximumSeconds(value);
	}

	bool isSexActCreatureMale(StaticFunctionTag *base) {
		return config.getSexActFlags().isCreatureMale();
	}

	void setSexActCreatureMale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setCreatureMale(value);
	}

	bool isSexActCreatureFemale(StaticFunctionTag *base) {
		return config.getSexActFlags().isCreatureFemale();
	}

	void setSexActCreatureFemale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setCreatureFemale(value);
	}

	bool isSexActGroupAllMale(StaticFunctionTag *base) {
		return config.getSexActFlags().isGroupAllMale();
	}

	void setSexActGroupAllMale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setGroupAllMale(value);
	}

	bool isSexActGroupOneFemale(StaticFunctionTag *base) {
		return config.getSexActFlags().isGroupOneFemale();
	}

	void setSexActGroupOneFemale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setGroupOneFemale(value);
	}

	bool isSexActGroupMixed(StaticFunctionTag *base) {
		return config.getSexActFlags().isGroupMixed();
	}

	void setSexActGroupMixed(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setGroupMixed(value);
	}

	bool isSexActGroupOneMale(StaticFunctionTag *base) {
		return config.getSexActFlags().isGroupOneMale();
	}

	void setSexActGroupOneMale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setGroupOneMale(value);
	}

	bool isSexActGroupAllFemale(StaticFunctionTag *base) {
		return config.getSexActFlags().isGroupAllFemale();
	}
	
	void setSexActGroupAllFemale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setGroupAllFemale(value);
	}

	bool isSexActCoupleAllMale(StaticFunctionTag *base) {
		return config.getSexActFlags().isCoupleAllMale();
	}

	void setSexActCoupleAllMale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setCoupleAllMale(value);
	}

	bool isSexActCoupleMixed(StaticFunctionTag *base) {
		return config.getSexActFlags().isCoupleMixed();
	}

	void setSexActCoupleMixed(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setCoupleMixed(value);
	}

	bool isSexActCoupleAllFemale(StaticFunctionTag *base) {
		return config.getSexActFlags().isCoupleAllFemale();
	}

	void setSexActCoupleAllFemale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setCoupleAllFemale(value);
	}

	bool isSexActSoloMale(StaticFunctionTag *base) {
		return config.getSexActFlags().isSoloMale();
	}

	void setSexActSoloMale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setSoloMale(value);
	}

	bool isSexActSoloFemale(StaticFunctionTag *base) {
		return config.getSexActFlags().isSoloFemale();
	}

	void setSexActSoloFemale(StaticFunctionTag *base, const bool value) {
		config.getSexActFlags().setSoloFemale(value);
	}

	bool isMaleMasturbatorCreatureMale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isCreatureMale();
	}

	void setMaleMasturbatorCreatureMale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setCreatureMale(value);
	}

	bool isMaleMasturbatorCreatureFemale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isCreatureFemale();
	}

	void setMaleMasturbatorCreatureFemale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setCreatureFemale(value);
	}

	bool isMaleMasturbatorGroupAllMale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isGroupAllMale();
	}

	void setMaleMasturbatorGroupAllMale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setGroupAllMale(value);
	}

	bool isMaleMasturbatorGroupOneFemale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isGroupOneFemale();
	}

	void setMaleMasturbatorGroupOneFemale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setGroupOneFemale(value);
	}

	bool isMaleMasturbatorGroupMixed(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isGroupMixed();
	}

	void setMaleMasturbatorGroupMixed(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setGroupMixed(value);
	}

	bool isMaleMasturbatorGroupOneMale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isGroupOneMale();
	}

	void setMaleMasturbatorGroupOneMale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setGroupOneMale(value);
	}

	bool isMaleMasturbatorGroupAllFemale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isGroupAllFemale();
	}

	void setMaleMasturbatorGroupAllFemale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setGroupAllFemale(value);
	}

	bool isMaleMasturbatorCoupleAllMale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isCoupleAllMale();
	}

	void setMaleMasturbatorCoupleAllMale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setCoupleAllMale(value);
	}

	bool isMaleMasturbatorCoupleMixed(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isCoupleMixed();
	}

	void setMaleMasturbatorCoupleMixed(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setCoupleMixed(value);
	}

	bool isMaleMasturbatorCoupleAllFemale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isCoupleAllFemale();
	}

	void setMaleMasturbatorCoupleAllFemale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setCoupleAllFemale(value);
	}

	bool isMaleMasturbatorSoloMale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isSoloMale();
	}

	void setMaleMasturbatorSoloMale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setSoloMale(value);
	}

	bool isMaleMasturbatorSoloFemale(StaticFunctionTag *base) {
		return config.getMaleMasturbatorFlags().isSoloFemale();
	}

	void setMaleMasturbatorSoloFemale(StaticFunctionTag *base, const bool value) {
		config.getMaleMasturbatorFlags().setSoloFemale(value);
	}

	bool isFemaleMasturbatorCreatureMale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isCreatureMale();
	}

	void setFemaleMasturbatorCreatureMale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setCreatureMale(value);
	}

	bool isFemaleMasturbatorCreatureFemale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isCreatureFemale();
	}

	void setFemaleMasturbatorCreatureFemale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setCreatureFemale(value);
	}

	bool isFemaleMasturbatorGroupAllMale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isGroupAllMale();
	}

	void setFemaleMasturbatorGroupAllMale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setGroupAllMale(value);
	}

	bool isFemaleMasturbatorGroupOneFemale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isGroupOneFemale();
	}

	void setFemaleMasturbatorGroupOneFemale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setGroupOneFemale(value);
	}

	bool isFemaleMasturbatorGroupMixed(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isGroupMixed();
	}

	void setFemaleMasturbatorGroupMixed(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setGroupMixed(value);
	}

	bool isFemaleMasturbatorGroupOneMale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isGroupOneMale();
	}

	void setFemaleMasturbatorGroupOneMale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setGroupOneMale(value);
	}

	bool isFemaleMasturbatorGroupAllFemale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isGroupAllFemale();
	}

	void setFemaleMasturbatorGroupAllFemale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setGroupAllFemale(value);
	}

	bool isFemaleMasturbatorCoupleAllMale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isCoupleAllMale();
	}

	void setFemaleMasturbatorCoupleAllMale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setCoupleAllMale(value);
	}

	bool isFemaleMasturbatorCoupleMixed(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isCoupleMixed();
	}

	void setFemaleMasturbatorCoupleMixed(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setCoupleMixed(value);
	}

	bool isFemaleMasturbatorCoupleAllFemale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isCoupleAllFemale();
	}

	void setFemaleMasturbatorCoupleAllFemale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setCoupleAllFemale(value);
	}

	bool isFemaleMasturbatorSoloMale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isSoloMale();
	}

	void setFemaleMasturbatorSoloMale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setSoloMale(value);
	}

	bool isFemaleMasturbatorSoloFemale(StaticFunctionTag *base) {
		return config.getFemaleMasturbatorFlags().isSoloFemale();
	}

	void setFemaleMasturbatorSoloFemale(StaticFunctionTag *base, const bool value) {
		config.getFemaleMasturbatorFlags().setSoloFemale(value);
	}

	void load(SKSESerializationInterface *serializer, UInt32 version) {
		config.load(serializer, version);
	}

	void save(SKSESerializationInterface *serializer) {
		config.save(serializer);
	}

	// Configuration functions are more or less atomic getters and setters that
	// should only be accessed via a single thread at a time. They are set up
	// with the NoWait attribute to improve performance.
	bool registerFunctions(VMClassRegistry *registry) {
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isQuestActive", "RndSexConfigQuest", isQuestActive, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setQuestActive", "RndSexConfigQuest", setQuestActive, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isShowNotifications", "RndSexConfigQuest", isShowNotifications, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setShowNotifications", "RndSexConfigQuest", setShowNotifications, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isAllowOldPeople", "RndSexConfigQuest", isAllowOldPeople, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setAllowOldPeople", "RndSexConfigQuest", setAllowOldPeople, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isAllowDragons", "RndSexConfigQuest", isAllowDragons, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setAllowDragons", "RndSexConfigQuest", setAllowDragons, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isAllowBeastRaces", "RndSexConfigQuest", isAllowBeastRaces, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setAllowBeastRaces", "RndSexConfigQuest", setAllowBeastRaces, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleInMalePosition", "RndSexConfigQuest", isMaleInMalePosition, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleInMalePosition", "RndSexConfigQuest", setMaleInMalePosition, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isUseSexLabBedSetting", "RndSexConfigQuest", isUseSexLabBedSetting, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setUseSexLabBedSetting", "RndSexConfigQuest", setUseSexLabBedSetting, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isCalmAfterSex", "RndSexConfigQuest", isCalmAfterSex, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setCalmAfterSex", "RndSexConfigQuest", setCalmAfterSex, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isPlayerOnly", "RndSexConfigQuest", isPlayerOnly, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setPlayerOnly", "RndSexConfigQuest", setPlayerOnly, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isNoTimeRestriction", "RndSexConfigQuest", isNoTimeRestriction, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setNoTimeRestriction", "RndSexConfigQuest", setNoTimeRestriction, registry));

		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, UInt32>("getFollowerOptionIndex", "RndSexConfigQuest", getFollowerOptionIndex, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, UInt32>("setFollowerOptionIndex", "RndSexConfigQuest", setFollowerOptionIndex, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, UInt32>("getArousalMinimum", "RndSexConfigQuest", getArousalMinimum, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, UInt32>("setArousalMinimum", "RndSexConfigQuest", setArousalMinimum, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, UInt32>("getMasturbatorMinimum", "RndSexConfigQuest", getMasturbatorMinimum, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, UInt32>("setMasturbatorMinimum", "RndSexConfigQuest", setMasturbatorMinimum, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, UInt32>("getMasturbatorMaximum", "RndSexConfigQuest", getMasturbatorMaximum, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, UInt32>("setMasturbatorMaximum", "RndSexConfigQuest", setMasturbatorMaximum, registry));

		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getActivityStartHour", "RndSexConfigQuest", getActivityStartHour, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setActivityStartHour", "RndSexConfigQuest", setActivityStartHour, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getActivityEndHour", "RndSexConfigQuest", getActivityEndHour, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setActivityEndHour", "RndSexConfigQuest", setActivityEndHour, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getMaxDistanceMeters", "RndSexConfigQuest", getMaxDistanceMeters, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setMaxDistanceMeters", "RndSexConfigQuest", setMaxDistanceMeters, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getPercentCreatureSex", "RndSexConfigQuest", getPercentCreatureSex, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setPercentCreatureSex", "RndSexConfigQuest", setPercentCreatureSex, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getPercentGroupSex", "RndSexConfigQuest", getPercentGroupSex, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setPercentGroupSex", "RndSexConfigQuest", setPercentGroupSex, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getPercentCoupleSex", "RndSexConfigQuest", getPercentCoupleSex, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setPercentCoupleSex", "RndSexConfigQuest", setPercentCoupleSex, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getPercentMasturbator", "RndSexConfigQuest", getPercentMasturbator, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setPercentMasturbator", "RndSexConfigQuest", setPercentMasturbator, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getPercentCreatureOnPlayer", "RndSexConfigQuest", getPercentCreatureOnPlayer, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setPercentCreatureOnPlayer", "RndSexConfigQuest", setPercentCreatureOnPlayer, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getPercentNpcOnPlayer", "RndSexConfigQuest", getPercentNpcOnPlayer, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setPercentNpcOnPlayer", "RndSexConfigQuest", setPercentNpcOnPlayer, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getIntervalMinimum", "RndSexConfigQuest", getIntervalMinimum, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getIntervalMinimumMinutes", "RndSexConfigQuest", getIntervalMinimumMinutes, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setIntervalMinimumMinutes", "RndSexConfigQuest", setIntervalMinimumMinutes, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getIntervalMinimumSeconds", "RndSexConfigQuest", getIntervalMinimumSeconds, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setIntervalMinimumSeconds", "RndSexConfigQuest", setIntervalMinimumSeconds, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getIntervalMaximum", "RndSexConfigQuest", getIntervalMaximum, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getIntervalMaximumMinutes", "RndSexConfigQuest", getIntervalMaximumMinutes, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setIntervalMaximumMinutes", "RndSexConfigQuest", setIntervalMaximumMinutes, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, float>("getIntervalMaximumSeconds", "RndSexConfigQuest", getIntervalMaximumSeconds, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, float>("setIntervalMaximumSeconds", "RndSexConfigQuest", setIntervalMaximumSeconds, registry));

		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActCreatureMale", "RndSexConfigQuest", isSexActCreatureMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActCreatureMale", "RndSexConfigQuest", setSexActCreatureMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActCreatureFemale", "RndSexConfigQuest", isSexActCreatureFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActCreatureFemale", "RndSexConfigQuest", setSexActCreatureFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActGroupAllMale", "RndSexConfigQuest", isSexActGroupAllMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActGroupAllMale", "RndSexConfigQuest", setSexActGroupAllMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActGroupOneFemale", "RndSexConfigQuest", isSexActGroupOneFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActGroupOneFemale", "RndSexConfigQuest", setSexActGroupOneFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActGroupMixed", "RndSexConfigQuest", isSexActGroupMixed, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActGroupMixed", "RndSexConfigQuest", setSexActGroupMixed, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActGroupOneMale", "RndSexConfigQuest", isSexActGroupOneMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActGroupOneMale", "RndSexConfigQuest", setSexActGroupOneMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActGroupAllFemale", "RndSexConfigQuest", isSexActGroupAllFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActGroupAllFemale", "RndSexConfigQuest", setSexActGroupAllFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActCoupleAllMale", "RndSexConfigQuest", isSexActCoupleAllMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActCoupleAllMale", "RndSexConfigQuest", setSexActCoupleAllMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActCoupleMixed", "RndSexConfigQuest", isSexActCoupleMixed, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActCoupleMixed", "RndSexConfigQuest", setSexActCoupleMixed, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActCoupleAllFemale", "RndSexConfigQuest", isSexActCoupleAllFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActCoupleAllFemale", "RndSexConfigQuest", setSexActCoupleAllFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActSoloMale", "RndSexConfigQuest", isSexActSoloMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActSoloMale", "RndSexConfigQuest", setSexActSoloMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isSexActSoloFemale", "RndSexConfigQuest", isSexActSoloFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setSexActSoloFemale", "RndSexConfigQuest", setSexActSoloFemale, registry));

		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorCreatureMale", "RndSexConfigQuest", isMaleMasturbatorCreatureMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorCreatureMale", "RndSexConfigQuest", setMaleMasturbatorCreatureMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorCreatureFemale", "RndSexConfigQuest", isMaleMasturbatorCreatureFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorCreatureFemale", "RndSexConfigQuest", setMaleMasturbatorCreatureFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorGroupAllMale", "RndSexConfigQuest", isMaleMasturbatorGroupAllMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorGroupAllMale", "RndSexConfigQuest", setMaleMasturbatorGroupAllMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorGroupOneFemale", "RndSexConfigQuest", isMaleMasturbatorGroupOneFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorGroupOneFemale", "RndSexConfigQuest", setMaleMasturbatorGroupOneFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorGroupMixed", "RndSexConfigQuest", isMaleMasturbatorGroupMixed, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorGroupMixed", "RndSexConfigQuest", setMaleMasturbatorGroupMixed, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorGroupOneMale", "RndSexConfigQuest", isMaleMasturbatorGroupOneMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorGroupOneMale", "RndSexConfigQuest", setMaleMasturbatorGroupOneMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorGroupAllFemale", "RndSexConfigQuest", isMaleMasturbatorGroupAllFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorGroupAllFemale", "RndSexConfigQuest", setMaleMasturbatorGroupAllFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorCoupleAllMale", "RndSexConfigQuest", isMaleMasturbatorCoupleAllMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorCoupleAllMale", "RndSexConfigQuest", setMaleMasturbatorCoupleAllMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorCoupleMixed", "RndSexConfigQuest", isMaleMasturbatorCoupleMixed, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorCoupleMixed", "RndSexConfigQuest", setMaleMasturbatorCoupleMixed, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorCoupleAllFemale", "RndSexConfigQuest", isMaleMasturbatorCoupleAllFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorCoupleAllFemale", "RndSexConfigQuest", setMaleMasturbatorCoupleAllFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorSoloMale", "RndSexConfigQuest", isMaleMasturbatorSoloMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorSoloMale", "RndSexConfigQuest", setMaleMasturbatorSoloMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isMaleMasturbatorSoloFemale", "RndSexConfigQuest", isMaleMasturbatorSoloFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setMaleMasturbatorSoloFemale", "RndSexConfigQuest", setMaleMasturbatorSoloFemale, registry));

		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorCreatureMale", "RndSexConfigQuest", isFemaleMasturbatorCreatureMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorCreatureMale", "RndSexConfigQuest", setFemaleMasturbatorCreatureMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorCreatureFemale", "RndSexConfigQuest", isFemaleMasturbatorCreatureFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorCreatureFemale", "RndSexConfigQuest", setFemaleMasturbatorCreatureFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorGroupAllMale", "RndSexConfigQuest", isFemaleMasturbatorGroupAllMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorGroupAllMale", "RndSexConfigQuest", setFemaleMasturbatorGroupAllMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorGroupOneFemale", "RndSexConfigQuest", isFemaleMasturbatorGroupOneFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorGroupOneFemale", "RndSexConfigQuest", setFemaleMasturbatorGroupOneFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorGroupMixed", "RndSexConfigQuest", isFemaleMasturbatorGroupMixed, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorGroupMixed", "RndSexConfigQuest", setFemaleMasturbatorGroupMixed, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorGroupOneMale", "RndSexConfigQuest", isFemaleMasturbatorGroupOneMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorGroupOneMale", "RndSexConfigQuest", setFemaleMasturbatorGroupOneMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorGroupAllFemale", "RndSexConfigQuest", isFemaleMasturbatorGroupAllFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorGroupAllFemale", "RndSexConfigQuest", setFemaleMasturbatorGroupAllFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorCoupleAllMale", "RndSexConfigQuest", isFemaleMasturbatorCoupleAllMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorCoupleAllMale", "RndSexConfigQuest", setFemaleMasturbatorCoupleAllMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorCoupleMixed", "RndSexConfigQuest", isFemaleMasturbatorCoupleMixed, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorCoupleMixed", "RndSexConfigQuest", setFemaleMasturbatorCoupleMixed, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorCoupleAllFemale", "RndSexConfigQuest", isFemaleMasturbatorCoupleAllFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorCoupleAllFemale", "RndSexConfigQuest", setFemaleMasturbatorCoupleAllFemale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorSoloMale", "RndSexConfigQuest", isFemaleMasturbatorSoloMale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorSoloMale", "RndSexConfigQuest", setFemaleMasturbatorSoloMale, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, bool>("isFemaleMasturbatorSoloFemale", "RndSexConfigQuest", isFemaleMasturbatorSoloFemale, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("setFemaleMasturbatorSoloFemale", "RndSexConfigQuest", setFemaleMasturbatorSoloFemale, registry));
		return true;
	}
}
