#pragma warning(disable: 4244)

#include <random>
#include "windows.h"
#include "RndSexCreatureAnim.h"
#include "RndSexCreatureMap.h"

// Defined in RndSexCreatureAnimWrapper.cpp
extern RndSexCreatureAnim creatureAnimations;

// Defined in RndSexEvaluator.cpp
extern std::default_random_engine random;

// Clear out this instance.
void RndSexCreatureMap::clear() {
	creatureRaceKeyMap.clear();
	creatureRaceKeyList.clear();
}

// Add a potential creature attacker.
void RndSexCreatureMap::addCreature(const char *raceKey, Actor *actor) {
	if ((strlen(raceKey) > 0) && (actor != nullptr)) {
		std::string raceKeyStr(raceKey);
		creatureRaceKeyMap[raceKeyStr].push_back(actor);
		creatureRaceKeyList.push_back(raceKey);
	}
}

// Return true if this instance contains no creatures.
bool RndSexCreatureMap::hasCreatures() const {
	return !creatureRaceKeyList.empty();
}

// Locate a random creature race key that also has available animations. Throws
// out_of_range if no creature race keys contain any animations.
const std::string RndSexCreatureMap::getValidRaceKey() {
	while (!creatureRaceKeyList.empty()) {
		size_t raceKeyIndex = 0;
		size_t raceKeyCount = creatureRaceKeyList.size();

		if (raceKeyCount > 1) {
			std::uniform_int_distribution<size_t> distribution(0, raceKeyCount - 1);
			raceKeyIndex = distribution(random);
		}

		std::string raceKey = creatureRaceKeyList[raceKeyIndex];

		if (creatureAnimations.hasRaceKey(raceKey)) {
			return raceKey;
		}

		creatureRaceKeyList.erase(std::remove(creatureRaceKeyList.begin(), creatureRaceKeyList.end(), raceKey), creatureRaceKeyList.end());
	}

	throw std::out_of_range("RndSexCreatureMap: No creature animations found for any race key.");
}

// Returns the creature list for the specified race key. Throws out_of_range if
// the entry is not found in the map. Note that a copy of the creature list is
// returned for thread safety.
const RndSexCreatureActorList RndSexCreatureMap::getCreaturesByRaceKey(std::string raceKey) const {
	return creatureRaceKeyMap.at(raceKey);
}
