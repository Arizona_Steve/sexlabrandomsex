#pragma once

#include "common/ITypes.h"
#include "skse64/PapyrusNativeFunctions.h"

namespace RndSexActivityWrapper {
	BSFixedString getMessage(StaticFunctionTag *base, VMArray<BSFixedString> actorNameArray, const bool masturbatingOnlooker);
	bool registerFunctions(VMClassRegistry* registry);
}
