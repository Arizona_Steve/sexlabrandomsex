#pragma once

#include <string>
#include <skse64\PapyrusArgs.h>
#include <skse64\ScaleformState.h>

/******************************************************************************/
/* Utility class to generate sex act messages. The messages are specified in  */
/* the translation file for each language and include {} placeholders to      */
/* indicate where actor names should be placed.                               */
/******************************************************************************/

class RndSexActivity {
private:
	BSScaleformTranslator* translator;
public:
	RndSexActivity();
	std::string getMessage(VMArray<BSFixedString>& actorNameArray, const bool masturbatingOnlooker);
private:
	BSScaleformTranslator* loadTranslator();
	std::string getTranslationTemplate(BSScaleformTranslator* translator, wchar_t* templateKey);
};
