#pragma once

#include <unordered_map>
#include "skse64/GameReferences.h"

typedef std::vector<std::string> RndSexCreatureRaceKeyList;
typedef std::vector<Actor *> RndSexCreatureActorList;
typedef std::unordered_map <std::string, RndSexCreatureActorList> RndSexCreatureRaceKeyMap;

class RndSexCreatureMap {
public:
	void clear();
	void addCreature(const char *raceKey, Actor *actor);
	bool hasCreatures() const;
	const std::string getValidRaceKey();
	const RndSexCreatureActorList getCreaturesByRaceKey(std::string raceKey) const;
private:
	RndSexCreatureRaceKeyMap creatureRaceKeyMap;
	RndSexCreatureRaceKeyList creatureRaceKeyList;
};
