#pragma warning(disable: 4244)

#include <random>
#include <string>
#include "windows.h"
#include "RndSexConfig.h"
#include "RndSexCreatureAnim.h"
#include "RndSexNpcAnim.h"
#include "RndSexEvaluator.h"

std::default_random_engine random;
std::uniform_real_distribution<float> percentDistribution(0.0F, 100.0F);

// Defined in RndSexConfigWrapper.cpp
extern RndSexConfig config;

// Defined in RndSexCreatureAnimWrapper.cpp
extern RndSexCreatureAnim creatureAnimations;

// Defined in RndSexNpcAnimWrapper.cpp
extern RndSexNpcAnim npcAnimations;

// Constructor.
RndSexEvaluator::RndSexEvaluator() :
	player(nullptr),
	playerFemale(false),
	usePlayer(false) {
}

// Clears out any entries from a previous evaluation.
void RndSexEvaluator::clearEvaluator() {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);
	player = nullptr;
	playerFemale = false;
	usePlayer = false;
	creatureEvaluator.clear();
	npcEvaluator.clear();
	sexActorArray.resize(0, nullptr);
}

// Adds the eligible player actor.
void RndSexEvaluator::addEligiblePlayer(Actor *actor, bool actorFemale) {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);
	player = actor;
	playerFemale = actorFemale;
}

// Adds a potential male attacker to the male attacker NPC list.
void RndSexEvaluator::addMaleNpcAttacker(Actor *actor) {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);
	npcEvaluator.addMaleAttacker(actor);
}

// Adds a potential male victim to the male victim NPC list.
void RndSexEvaluator::addMaleNpcVictim(Actor *actor) {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);
	npcEvaluator.addMaleVictim(actor);
}

// Adds a potential female attacker to the female attacker NPC list.
void RndSexEvaluator::addFemaleNpcAttacker(Actor *actor) {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);
	npcEvaluator.addFemaleAttacker(actor);
}

// Adds a potential female victim to the female victim NPC list.
void RndSexEvaluator::addFemaleNpcVictim(Actor *actor) {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);
	npcEvaluator.addFemaleVictim(actor);
}

// Adds a potential creature attacker to the creature list.
void RndSexEvaluator::addCreature(BSFixedString &raceKey, Actor *actor) {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);
	creatureEvaluator.addCreature(raceKey.data, actor);
}

// Set up a sex act.
void RndSexEvaluator::evaluate(const bool creaturesAllowed) {
	std::unique_lock<std::shared_mutex> lock(evaluatorMutex);

	if ((player == nullptr) && config.isPlayerOnly()) {
		return;
	}

	if (!evaluateCreatureSex(creaturesAllowed) && !npcEvaluator.evaluateGroupSex(sexActorArray, player, playerFemale) && !npcEvaluator.evaluateCoupleSex(sexActorArray, player, playerFemale)) {
		npcEvaluator.evaluateSoloSex(sexActorArray);
	}
}

// Used by Papyrus to return the sex act participant array.
VMResultArray<Actor*> RndSexEvaluator::getSexActorArray() {
	std::shared_lock<std::shared_mutex> lock(evaluatorMutex);
	return sexActorArray;
}

// Used by Papyrus to return the evaluated masturbator array.
VMResultArray<Actor*> RndSexEvaluator::getMasturbatorArray() {
	std::shared_lock<std::shared_mutex> lock(evaluatorMutex);
	return npcEvaluator.getMasturbatorArray();
}

// Set up a creature sex act and masturbating onlookers. Placed here as the
// masturbating onlooker and victim searches use the NPC evaluator.
bool RndSexEvaluator::evaluateCreatureSex(const bool creaturesAllowed) {

	if (creaturesAllowed && creatureEvaluator.canEvaluate(player, playerFemale, npcEvaluator.getMaleCount(), npcEvaluator.getFemaleCount(), usePlayer)) {
		try {
			bool victimFemale = playerFemale;
			RndSexConfigFlags& sexActFlags = config.getSexActFlags();
			bool selectMale = sexActFlags.isCreatureMale();
			bool selectFemale = sexActFlags.isCreatureFemale();
			Actor* victim = usePlayer ? player : npcEvaluator.selectNpc(selectMale, selectFemale, selectMale, selectFemale, victimFemale);
			std::string raceKey = creatureEvaluator.getValidRaceKey();
			UInt32 creatureCount = creatureEvaluator.determineCount(raceKey);
			creatureEvaluator.selectAttackers(sexActorArray, raceKey, creatureCount, victim);
			npcEvaluator.selectMasturbatorsForCreatureSex(sexActorArray, victimFemale);
			return true;
		} catch (...) {
			sexActorArray.resize(0, nullptr);
			npcEvaluator.restoreLists();
		}
	}

	return false;
}
