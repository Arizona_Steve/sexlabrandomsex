#pragma warning(disable: 4244)
#pragma warning(disable: 4267)

#include <string>
#include "windows.h"
#include "common/ITypes.h"
#include "RndSexEvaluator.h"
#include "RndSexEvaluatorWrapper.h"

RndSexEvaluator evaluator;

namespace RndSexEvaluatorWrapper {
	void clearEvaluator(StaticFunctionTag *base) {
		evaluator.clearEvaluator();
	}

	void addEligiblePlayer(StaticFunctionTag *base, Actor *actor, bool actorFemale) {
		evaluator.addEligiblePlayer(actor, actorFemale);
	}

	void addMaleNpcAttacker(StaticFunctionTag *base, Actor *actor) {
		evaluator.addMaleNpcAttacker(actor);
	}

	void addMaleNpcVictim(StaticFunctionTag *base, Actor *actor) {
		evaluator.addMaleNpcVictim(actor);
	}

	void addFemaleNpcAttacker(StaticFunctionTag *base, Actor *actor) {
		evaluator.addFemaleNpcAttacker(actor);
	}

	void addFemaleNpcVictim(StaticFunctionTag *base, Actor *actor) {
		evaluator.addFemaleNpcVictim(actor);
	}

	void addCreature(StaticFunctionTag *base, BSFixedString raceKey, Actor *actor) {
		evaluator.addCreature(raceKey, actor);
	}

	void evaluate(StaticFunctionTag *base, bool creaturesAllowed) {
		evaluator.evaluate(creaturesAllowed);
	}

	VMResultArray<Actor*> getSexActorArray(StaticFunctionTag *base) {
		return evaluator.getSexActorArray();
	}

	VMResultArray<Actor*> getMasturbatorArray(StaticFunctionTag *base) {
		return evaluator.getMasturbatorArray();
	}

	bool registerFunctions(VMClassRegistry *registry) {
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, void>("clearEvaluator", "RndSexEvaluatorQuest", clearEvaluator, registry));
		registry->RegisterFunction(new NativeFunction2<StaticFunctionTag, void, Actor *, bool>("addEligiblePlayer", "RndSexEvaluatorQuest", addEligiblePlayer, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, Actor *>("addMaleNpcAttacker", "RndSexEvaluatorQuest", addMaleNpcAttacker, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, Actor *>("addMaleNpcVictim", "RndSexEvaluatorQuest", addMaleNpcVictim, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, Actor *>("addFemaleNpcAttacker", "RndSexEvaluatorQuest", addFemaleNpcAttacker, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, Actor *>("addFemaleNpcVictim", "RndSexEvaluatorQuest", addFemaleNpcVictim, registry));
		registry->RegisterFunction(new NativeFunction2<StaticFunctionTag, void, BSFixedString, Actor *>("addCreature", "RndSexEvaluatorQuest", addCreature, registry));
		registry->RegisterFunction(new NativeFunction1<StaticFunctionTag, void, bool>("evaluate", "RndSexEvaluatorQuest", evaluate, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, VMResultArray<Actor*>>("getSexActorArray", "RndSexEvaluatorQuest", getSexActorArray, registry));
		registry->RegisterFunction(new NativeFunction0<StaticFunctionTag, VMResultArray<Actor*>>("getMasturbatorArray", "RndSexEvaluatorQuest", getMasturbatorArray, registry));

		registry->SetFunctionFlags("RndSexEvaluatorQuest", "clearEvaluator", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "addEligiblePlayer", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "addMaleNpcAttacker", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "addMaleNpcVictim", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "addFemaleNpcAttacker", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "addFemaleNpcVictim", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "addCreature", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "evaluate", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "getSexActorArray", VMClassRegistry::kFunctionFlag_NoWait);
		registry->SetFunctionFlags("RndSexEvaluatorQuest", "getMasturbatorArray", VMClassRegistry::kFunctionFlag_NoWait);
		return true;
	}
}
