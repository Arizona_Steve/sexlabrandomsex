#pragma once

#include "skse64/GameReferences.h"
#include "skse64/PapyrusNativeFunctions.h"

namespace RndSexEvaluatorWrapper {
	void clearEvaluator(StaticFunctionTag *base);
	void addEligiblePlayer(StaticFunctionTag *base, Actor *actor, bool actorFemale);
	void addMaleNpcAttacker(StaticFunctionTag *base, Actor *actor);
	void addMaleNpcVictim(StaticFunctionTag *base, Actor *actor);
	void addFemaleNpcAttacker(StaticFunctionTag *base, Actor *actor);
	void addFemaleNpcVictim(StaticFunctionTag *base, Actor *actor);
	void addCreature(StaticFunctionTag *base, BSFixedString raceKey, Actor *actor);
	void evaluate(StaticFunctionTag *base, bool creaturesAllowed);
	VMResultArray<Actor *> getSexActorArray(StaticFunctionTag *base);
	VMResultArray<Actor *> getMasturbatorArray(StaticFunctionTag *base);
	bool registerFunctions(VMClassRegistry* registry);
}
